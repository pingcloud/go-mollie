package services

import (
	"github.com/icrowley/fake"
	"bitbucket.org/pingcloud/go-mollie/resources"
	"bitbucket.org/pingcloud/go-mollie/types"
	"os"
	"strings"
	"testing"
	"time"
)

func TestPaymentsEndpoint_Create(t *testing.T) {
	payments := NewPaymentsEndpoint(os.Getenv("MOLLIE_API_KEY"))
	req := CreatePaymentParams{
		Method:      types.MethodCreditcard,
		Amount:      *types.NewAmount(100, types.CurrencyEUR),
		Description: "TestPayment " + time.Now().Format(time.RFC3339),
		RedirectURL: "https://webstollen.de/test",
	}
	payment, _, err := payments.Create(req)
	if err != nil {
		t.Fatal(err)
	}
	if payment.IsCancelable {
		defer func() {
			_, _, err = payments.Cancel(payment.ID)
			if err != nil {
				t.Fatal(err)
			}
		}()
	}
	if strings.Index(payment.ID, "tr_") != 0 {
		t.Errorf("Payment ID should start with 'tr_', but is '%s'", payment.ID)
	}
}

func TestPaymentsEndpoint_Get(t *testing.T) {
	payments := NewPaymentsEndpoint(os.Getenv("MOLLIE_API_KEY"))
	payment1 := createPayment(t)

	if payment1.IsCancelable {
		defer func() {
			_, _, err := payments.Cancel(payment1.ID)
			if err != nil {
				t.Fatal(err)
			}
		}()
	}

	payment2, _, err := payments.Get(payment1.ID, GetPaymentParams{
		Include: "refunds,chargebacks",
	})
	if err != nil {
		t.Fatal(err)
	}
	if payment2.ID != "" && payment1.ID != payment2.ID {
		t.Fatalf("Expected ayment ID '%s', but got '%s'", payment1.ID, payment2.ID)
	}
}

func TestPaymentsEndpoint_List(t *testing.T) {
	payments := NewPaymentsEndpoint(os.Getenv("MOLLIE_API_KEY"))
	_ = createPayment(t)
	_ = createPayment(t)

	list, _, err := payments.List(ListParams{Limit: 2})
	if err != nil {
		t.Fatal(err)
	}

	if list.Count != uint(len(list.Embedded.Payments)) {
		t.Fatalf("Expected %d payments, got %d", list.Count, len(list.Embedded.Payments))
	}

	if list.Count < 2 {
		t.Fatalf("Expected 2 payments, got %d", list.Count)
	}

}

func TestPaymentsEndpoint_Cancel(t *testing.T) {
	payments := NewPaymentsEndpoint(os.Getenv("MOLLIE_API_KEY"))
	req := CreatePaymentParams{
		Method:       types.MethodBanktransfer,
		Amount:       *types.NewAmount(100, types.CurrencyEUR),
		Description:  "TestPayment " + time.Now().Format(time.RFC3339),
		RedirectURL:  "https://webstollen.de/test",
		Locale:       types.LocaleDeDe,
		BillingEmail: fake.UserName() + "@mollie.webstollen.de",
	}
	payment, _, err := payments.Create(req)
	if err != nil {
		t.Fatal(err)
	}

	if !payment.IsCancelable {
		t.Fatal("Expected payment to be cancelable")
	}

	_, _, err = payments.Cancel(payment.ID)
	if err != nil {
		t.Fatal(err)
	}

}

func TestPaymentsEndpoint_Errors(t *testing.T) {
	payments := NewPaymentsEndpoint(os.Getenv("MOLLIE_API_KEY"))

	_, _, err := payments.Create(CreatePaymentParams{})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = payments.Get("xxx_12345", GetPaymentParams{})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = payments.List(ListParams{From: "xxx_12345", Limit: 0})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = payments.Cancel("xxx_1234")
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}
}

func createPayment(t *testing.T) *resources.Payment {
	payments := NewPaymentsEndpoint(os.Getenv("MOLLIE_API_KEY"))
	req := CreatePaymentParams{
		Method:      types.MethodCreditcard,
		Amount:      *types.NewAmount(100, types.CurrencyEUR),
		Description: "TestPayment " + time.Now().Format(time.RFC3339),
		RedirectURL: "https://webstollen.de/test",
	}
	payment, _, err := payments.Create(req)
	if err != nil {
		t.Fatal(err)
	}
	return payment
}
