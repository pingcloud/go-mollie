package services

import (
	"github.com/icrowley/fake"
	"bitbucket.org/pingcloud/go-mollie/resources"
	"bitbucket.org/pingcloud/go-mollie/types"
	"os"
	"strings"
	"testing"
)

func TestMandatesEndpoint_Create(t *testing.T) {
	mandates := NewMandatesEndpoint(os.Getenv("MOLLIE_API_KEY"))
	customers := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))

	customer, err := createCustomer()
	if err != nil {
		t.Fatal("Could not create Customer:", err.Error())
	}

	defer func() {
		_, err = customers.Delete(customer.ID)
		if err != nil {
			t.Error(err)
		}
	}()

	// http://ibanvalidieren.de/beispiele.html
	mandateReq := CreateMandateParams{
		Method:          resources.MandatemethodDirectdebit,
		ConsumerName:    customer.Name,
		ConsumerAccount: "DE02120300000000202051",
		ConsumerBIC:     "BYLADEM1001",
	}
	mandate, err := createMandate(customer, mandateReq)
	if err != nil {
		t.Fatal("Could not create Mandate:", err.Error())
	}

	defer func() {
		_, err = mandates.Revoke(customer.ID, mandate.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()

	if strings.Index(mandate.ID, "mdt_") != 0 {
		t.Fatalf("Mandate ID should start with 'mdt_', but is '%s'", mandate.ID)
	}
}

func TestMandatesEndpoint_Get(t *testing.T) {
	mandates := NewMandatesEndpoint(os.Getenv("MOLLIE_API_KEY"))
	customers := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))

	customer, err := createCustomer()
	if err != nil {
		t.Fatal("Could not create Customer:", err.Error())
	}

	defer func() {
		_, err = customers.Delete(customer.ID)
		if err != nil {
			t.Error(err)
		}
	}()

	mandateReq := CreateMandateParams{
		Method:          resources.MandatemethodDirectdebit,
		ConsumerName:    customer.Name,
		ConsumerAccount: "DE02120300000000202051",
		ConsumerBIC:     "BYLADEM1001",
	}
	mandate, err := createMandate(customer, mandateReq)
	if err != nil {
		t.Fatal("Could not create Mandate:", err.Error())
	}

	if strings.Index(mandate.ID, "mdt_") != 0 {
		t.Fatalf("Mandate ID should start with 'mdt_', but is '%s'", mandate.ID)
	}

	defer func() {
		_, err = mandates.Revoke(customer.ID, mandate.ID)
		if err != nil {
			t.Error(err)
		}
	}()

	mandate2, _, err := mandates.Get(customer.ID, mandate.ID)
	if err != nil {
		t.Fatal("Could not get Mandate:", err.Error())
	}

	if strings.Index(mandate2.ID, "mdt_") != 0 {
		t.Fatalf("Mandate ID should start with 'mdt_', but is '%s'", mandate.ID)
	}

	if mandate2.ID != "" && mandate.ID != mandate2.ID {
		t.Fatalf("Mandate ID should be identical: '%s', '%s'", mandate.ID, mandate2.ID)
	}
}

func TestMandatesEndpoint_Revoke(t *testing.T) {
	customers := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))
	mandates := NewMandatesEndpoint(os.Getenv("MOLLIE_API_KEY"))

	customer, err := createCustomer()
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		_, err = customers.Delete(customer.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()

	mandateReq := CreateMandateParams{
		Method:          resources.MandatemethodDirectdebit,
		ConsumerName:    customer.Name,
		ConsumerAccount: "DE02120300000000202051",
		ConsumerBIC:     "BYLADEM1001",
	}
	mandate, err := createMandate(customer, mandateReq)
	if err != nil {
		t.Fatal(err)
	}

	_, err = mandates.Revoke(customer.ID, mandate.ID)
	if err != nil {
		t.Fatal(err)
	}
	_, err = mandates.Revoke(customer.ID, mandate.ID)
	if err == nil {
		t.Fatal("Repeated revocation should result in an error, it didn't!")
	}

}

func TestMandatesEndpoint_List(t *testing.T) {
	mandates := NewMandatesEndpoint(os.Getenv("MOLLIE_API_KEY"))
	customers := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))
	customer, err := createCustomer()
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		_, err = customers.Delete(customer.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()

	mandateReq1 := CreateMandateParams{
		Method:          resources.MandatemethodDirectdebit,
		ConsumerName:    customer.Name,
		ConsumerAccount: "DE02120300000000202051",
		ConsumerBIC:     "BYLADEM1001",
	}
	mandateReq2 := CreateMandateParams{
		Method:          resources.MandatemethodDirectdebit,
		ConsumerName:    customer.Name,
		ConsumerAccount: "DE02500105170137075030",
		ConsumerBIC:     "INGDDEFF",
	}

	mandate1, err := createMandate(customer, mandateReq1)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		_, err = mandates.Revoke(customer.ID, mandate1.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()

	mandate2, err := createMandate(customer, mandateReq2)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		_, err = mandates.Revoke(customer.ID, mandate2.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()

	mandatesList1, _, err := mandates.List(customer.ID, ListParams{})
	if len(mandatesList1.Embedded.Mandates) != 2 {
		t.Fatalf("Should get 2 Mandates, got: %d", len(mandatesList1.Embedded.Mandates))
	}

	mandatesList2, _, err := mandates.List(customer.ID, ListParams{From: mandate1.ID, Limit: 1})
	if len(mandatesList2.Embedded.Mandates) != 1 {
		t.Fatalf("Should get 1 Mandates, got: %d", len(mandatesList2.Embedded.Mandates))
	}
}

func TestMandatesEndpoint_Errors(t *testing.T) {
	mandates := NewMandatesEndpoint(os.Getenv("MOLLIE_API_KEY"))

	_, _, err := mandates.List("yyy_123455", ListParams{From: "xxx_12345", Limit: 0})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = mandates.Get("xxx_123456", "yyy_123456")
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = mandates.Create("xxx_12354", CreateMandateParams{})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, err = mandates.Revoke("xxx_123456", "yyy_123456")
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

}

func createMandate(customer *resources.Customer, mandateReq CreateMandateParams) (resources.Mandate, error) {
	mandates := NewMandatesEndpoint(os.Getenv("MOLLIE_API_KEY"))
	mandate, _, err := mandates.Create(customer.ID, mandateReq)
	return mandate, err
}

func createCustomer() (*resources.Customer, error) {
	customers := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))
	customerReq := CreateCustomerParams{
		Locale: types.LocaleDeDe,
		Email:  fake.UserName() + "@mollie.webstollen.de",
		Name:   fake.FirstName() + " " + fake.LastName(),
	}
	customer, _, err := customers.Create(customerReq)
	return customer, err
}
