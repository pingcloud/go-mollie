package services

import (
	"fmt"
	"github.com/dghubble/sling"
	"bitbucket.org/pingcloud/go-mollie/resources"
	"bitbucket.org/pingcloud/go-mollie/types"
	"net/http"
)

// PaymentsEndpoint - https://docs.mollie.com/payments/overview
// The Payments API allows you to create payments for your web shop, e-invoicing platform or other application that you
// need payments for.
type PaymentsEndpoint struct {
	client *sling.Sling
}

// NewPaymentsEndpoint returns a new PaymentsEndpoint
func NewPaymentsEndpoint(apiKey string) *PaymentsEndpoint {
	return &PaymentsEndpoint{
		client: NewClient(apiKey),
	}
}

// Create - https://docs.mollie.com/reference/v2/payments-api/create-payment
// Payment creation is elemental to the Mollie API: this is where most payment implementations start off.
// Once you have created a payment, you should redirect your customer to the URL in the _links.checkout property from
// the response.
// To wrap your head around the payment process, an explanation and flow charts can be found in the
// Payments API Overview.
func (e *PaymentsEndpoint) Create(params CreatePaymentParams) (*resources.Payment, *http.Response, error) {
	payment := new(resources.Payment)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Post("payments?include=details.qrCode").BodyJSON(params).Receive(payment, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return payment, resp, err
}

// Get - https://docs.mollie.com/reference/v2/payments-api/get-payment
// Retrieve a single payment object by its payment token.
func (e *PaymentsEndpoint) Get(paymentID string, params GetPaymentParams) (*resources.Payment, *http.Response, error) {
	payment := new(resources.Payment)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Get(fmt.Sprintf("payments/%s", paymentID)).QueryStruct(params).Receive(payment, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return payment, resp, err
}

// Cancel - https://docs.mollie.com/reference/v2/payments-api/cancel-payment
// Some payment methods are cancellable for an amount of time, usually until the next day. Or as long as the payment
// status is open. Payments may be canceled manually from the Dashboard, or automatically by using this endpoint.
// The isCancelable property on the Payment object will indicate if the payment can be canceled.
func (e *PaymentsEndpoint) Cancel(paymentID string) (*resources.Payment, *http.Response, error) {
	payment := new(resources.Payment)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Delete(fmt.Sprintf("payments/%s", paymentID)).Receive(payment, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return payment, resp, err
}

// List - https://docs.mollie.com/reference/v2/payments-api/list-payments
// Retrieve all payments created with the current website profile, ordered from newest to oldest.
// The results are paginated. See pagination for more information.
func (e *PaymentsEndpoint) List(params ListParams) (*resources.PaymentList, *http.Response, error) {
	mollieError := new(resources.Error)
	payments := new(resources.PaymentList)
	resp, err := e.client.New().Get("payments").QueryStruct(params).Receive(payments, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return payments, resp, err
}

// CreatePaymentParams - https://docs.mollie.com/reference/v2/payments-api/create-payment#parameters
type CreatePaymentParams struct {
	// Amount - The amount that you want to charge, e.g. {"currency":"EUR", "value":"100.00"} if you would want to
	// charge €100.00.
	Amount types.Amount `json:"amount"`

	// The description of the payment you’re creating. This will be shown to your customer on their card or bank
	// statement when possible. We truncate the description automatically according to the limits of the used payment
	// method. The description is also visible in any exports you generate.
	//
	// We recommend you use a unique identifier so that you can always link the payment to the order in your back
	// office. This is particularly useful for bookkeeping.
	Description string `json:"description"`

	// The URL your customer will be redirected to after the payment process.
	//
	// Only for payments with the sequenceType parameter set to recurring, you can omit this parameter. For all other
	// payments, this parameter is mandatory.
	//
	// It could make sense for the redirectUrl to contain a unique identifier – like your order ID – so you can show the
	// right page referencing the order when your customer returns.
	RedirectURL string `json:"redirectUrl,omitempty"`

	// Set the webhook URL, where we will send payment status updates to.
	WebhookURL string `json:"webhookUrl,omitempty"`

	// Allows you to preset the language to be used in the hosted payment pages shown to the consumer. Setting a locale
	// is highly recommended and will greatly improve your conversion rate. When this parameter is omitted, the browser
	// language will be used instead if supported by the payment method. You can provide any ISO 15897 locale, but our
	// hosted payment pages currently only support the following languages:
	Locale types.Locale `json:"locale,omitempty"`

	// Normally, a payment method screen is shown. However, when using this parameter, you can choose a specific payment
	// method and your customer will skip the selection screen and is sent directly to the chosen payment method. The
	// parameter enables you to fully integrate the payment method selection into your website.
	//
	// You can also specify the methods in an array. By doing so we will still show the payment method selection screen
	// but will only show the methods specified in the array. For example, you can use this functionality to only show
	// payment methods from a specific country to your customer ['bancontact', 'belfius', 'inghomepay'].
	Method types.Method `json:"method,omitempty"`

	// Provide any data you like, for example a string or a JSON object. We will save the data alongside the payment.
	// Whenever you fetch the payment with our API, we’ll also include the metadata. You can use up to approximately
	// 1kB.
	Metadata map[string]interface{} `json:"metadata,omitempty"`

	// Indicate which type of payment this is in a recurring sequence. If set to first, a first payment is created for
	// the customer, allowing the customer to agree to automatic recurring charges taking place on their account in the
	// future. If set to recurring, the customer’s card is charged automatically.
	//
	// Defaults to oneoff, which is a regular non-recurring payment (see also: Recurring).
	SequenceType types.SequenceType `json:"sequenceType,omitempty"`

	// The ID of the Customer for whom the payment is being created. This is used for recurring payments and single
	// click payments.
	CustomerID string `json:"customerId,omitempty"`

	// When creating recurring payments, the ID of a specific Mandate may be supplied to indicate which of the
	// consumer’s accounts should be credited.
	MandateID string `json:"mandateId,omitempty"`

	// Consumer’s email address, to automatically send the bank transfer details to. Please note: the payment
	// instructions will be sent immediately when creating the payment. If you don’t specify the locale parameter, the
	// email will be sent in English, as we haven’t yet been able to detect the consumer’s browser language.
	//
	// User for: banktransfer, przelewy24
	BillingEmail string `json:"billingEmail,omitempty"`

	// The date the payment should expire, in YYYY-MM-DD format. Please note: the minimum date is tomorrow and the
	// maximum date is 100 days after tomorrow.
	//
	// User for: banktransfer
	DueDate types.Date `json:"dueDate,omitempty"`

	// The card holder’s address details. We advise to provide these details to improve the credit card fraud
	// protection, and thus improve conversion.
	//
	// Used for: creditcard
	BillingAddress types.Address `json:"billingAddress,omitempty"`

	// The shipping address details. We advise to provide these details to improve the credit card fraud protection, and
	// thus improve conversion.
	//
	// Used for: creditcard, paypal
	ShippingAddress types.Address `json:"shippingAddress,omitempty"`

	// The gift card brand to use for the payment. These issuers can be retrieved by using the issuers include in the
	// Methods API. If you need a brand not in the list, contact our support department. We can also support closed-loop
	// cards.
	//
	// If only one issuer is activated on your account, you can omit this parameter.
	// Used for: giftcards
	GiftCardIssuer types.GiftCardIssuer `json:"issuer,omitempty"`

	// The card number on the gift card.
	//
	// Used for: giftcard
	VoucherNumber string `json:"voucherNumber,omitempty"`

	// The PIN code on the gift card. Only required if there is a PIN code printed on the gift card.
	//
	// Used for: giftcard
	VoucherPin string `json:"voucherPin,omitempty"`

	// An iDEAL issuer ID, for example ideal_INGBNL2A. The returned payment URL will deep-link into the specific banking
	// website (ING Bank, in this example). The full list of issuers can be retrieved via the Methods API by using the
	// optional issuers include.
	//
	// Used for: ideal
	IDEALIssuer string `json:"issuer,omitempty"`

	// The issuer to use for the KBC/CBC payment.The full list of issuers can be retrieved via the Methods API by using
	// the optional issuers include.
	//
	// Used for: kbc
	KBCCBCIssuer types.KBCCBCIssuer `json:"issuer,omitempty"`

	// Used for consumer identification. For example, you could use the consumer’s IP address.
	//
	// Used for: paysafecard
	CustomerReference string `json:"customerReference,omitempty"`

	// Beneficiary name of the account holder. Only available if one-off payments are enabled on your account. Will
	// pre-fill the beneficiary name in the checkout screen if present.
	//
	// Used for: directdebit
	ConsumerName string `json:"consumerName,omitempty"`

	// IBAN of the account holder. Only available if one-off payments are enabled on your account. Will pre-fill the
	// IBAN in the checkout screen if present.
	//
	// Used for: directdebit
	ConsumerAccount string `json:"consumerAccount,omitempty"`
}

// GetPaymentParams - https://docs.mollie.com/reference/v2/payments-api/get-payment#parameters
type GetPaymentParams struct {
	// Include - This endpoint allows you to include additional information by appending the following values via the
	// include querystring parameter.
	Include string `json:"include,omitempty" url:"include,omitempty"`

	// This endpoint also allows for embedding additional information by appending the following values via the embed
	// query string parameter.
	Embed string `json:"embed,omitempty" url:"embed,omitempty"`
}
