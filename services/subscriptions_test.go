package services

import (
	"fmt"
	"bitbucket.org/pingcloud/go-mollie/resources"
	"bitbucket.org/pingcloud/go-mollie/types"
	"math/rand"
	"os"
	"strings"
	"testing"
	"time"
)

func TestSubscriptionEnpoint_Create(t *testing.T) {
	customers := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))
	subscriptions := NewSubscriptionEndpoint(os.Getenv("MOLLIE_API_KEY"))
	mandates := NewMandatesEndpoint(os.Getenv("MOLLIE_API_KEY"))
	customer, err := createCustomer()
	if err != nil {
		t.Fatal("Could not create Customer:", err.Error())
	}
	defer func() {
		_, err = customers.Delete(customer.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()
	mandat, _, err := mandates.Create(customer.ID, CreateMandateParams{
		Method:           resources.MandatemethodDirectdebit,
		ConsumerName:     customer.Name,
		ConsumerAccount:  "DE02120300000000202051",
		ConsumerBIC:      "BYLADEM1001",
		MandateReference: "GoMollieTest" + time.Now().Format(time.RFC3339),
		SignatureDate:    types.NewDate(time.Now()),
	})
	if err != nil {
		t.Fatal(err)
	}

	subscription, _, err := subscriptions.Create(customer.ID, CreateSubscriptionParams{
		Amount:      *types.NewAmount(100.0, types.CurrencyEUR),
		Times:       10,
		Interval:    types.NewInterval(1, types.IntervalperiodMonths),
		StartDate:   types.NewDate(time.Now()),
		Description: fmt.Sprintf("TEST-Subscription for %s, (%s)", customer.Name, customer.ID),
		MandateID:   mandat.ID,
	})
	if err != nil {
		t.Fatal(err)
	}
	if strings.Index(subscription.ID, "sub_") != 0 {
		t.Errorf("Subscription ID should start with 'sub_', but is '%s'", subscription.ID)
	}
}

func TestSubscriptionEnpoint_Cancel(t *testing.T) {
	customers := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))
	subscriptions := NewSubscriptionEndpoint(os.Getenv("MOLLIE_API_KEY"))
	customer, err := createCustomer()
	if err != nil {
		t.Fatal("Could not create Customer:", err.Error())
	}
	defer func() {
		_, err = customers.Delete(customer.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()
	subs1 := createSubscription(t, customer)

	subs2, _, err := subscriptions.Cancel(customer.ID, subs1.ID)
	if err != nil {
		t.Fatal(err)
	}

	if subs2.Status != resources.SubscriptionstatusCanceled {
		t.Fatalf("Expected status '%s', got '%s'", resources.SubscriptionstatusCanceled, subs2.Status)
	}
}

func TestSubscriptionEnpoint_Update(t *testing.T) {
	customers := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))
	subscriptions := NewSubscriptionEndpoint(os.Getenv("MOLLIE_API_KEY"))
	customer, err := createCustomer()
	if err != nil {
		t.Fatal("Could not create Customer:", err.Error())
	}
	defer func() {
		_, err = customers.Delete(customer.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()
	subs1 := createSubscription(t, customer)
	subs2, _, err := subscriptions.Update(customer.ID, subs1.ID, UpdateSubscriptionParams{
		Times:  20,
		Amount: *types.NewAmount(10, types.CurrencyEUR),
		Metadata: map[string]interface{}{
			"TEST": "VALUE",
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	if subs1.ID != subs2.ID || subs2.Times != 20 {
		t.Fatalf("Expected times to be 20, got %d", subs2.Times)
	}
}

func TestSubscriptionEnpoint_List(t *testing.T) {
	customers := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))
	subscriptions := NewSubscriptionEndpoint(os.Getenv("MOLLIE_API_KEY"))
	customer, err := createCustomer()
	if err != nil {
		t.Fatal("Could not create Customer:", err.Error())
	}
	defer func() {
		_, err = customers.Delete(customer.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()
	subs1 := createSubscription(t, customer)
	subs2 := createSubscription(t, customer)
	if subs1.ID == subs2.ID {
		t.Fatal("Unable to create to individual subscriptions")
	}
	list, _, err := subscriptions.List(customer.ID, ListParams{Limit: 2})
	if list.Count != 2 || list.Count != uint(len(list.Embedded.Subscriptions)) {
		t.Fatalf("Expected %d subscriptions, got %d", list.Count, len(list.Embedded.Subscriptions))
	}
}

func TestSubscriptionEnpoint_Get(t *testing.T) {
	customers := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))
	subscriptions := NewSubscriptionEndpoint(os.Getenv("MOLLIE_API_KEY"))
	customer, err := createCustomer()
	if err != nil {
		t.Fatal("Could not create Customer:", err.Error())
	}
	defer func() {
		_, err = customers.Delete(customer.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()
	subs1 := createSubscription(t, customer)
	subs2, _, err := subscriptions.Get(customer.ID, subs1.ID)
	if err != nil {
		t.Fatal(err)
	}
	if subs2.ID != "" && subs1.ID != subs2.ID {
		t.Fatalf("Expexted ID to be %s, got %s", subs1.ID, subs2.ID)
	}
}

func TestSubscriptionEnpoint_Errors(t *testing.T) {

	subscriptions := NewSubscriptionEndpoint(os.Getenv("MOLLIE_API_KEY"))

	_, _, err := subscriptions.Create("xxx_1232456", CreateSubscriptionParams{})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = subscriptions.Get("xxx_1232456", "yyy_123456")
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = subscriptions.Update("xxx_123456", "yyy_123456", UpdateSubscriptionParams{})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = subscriptions.Cancel("xxx_1232456", "yyy_123456")
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = subscriptions.List("yyy_123456", ListParams{})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

}

func createSubscription(t *testing.T, customer *resources.Customer) *resources.Subscription {

	rand.Seed(time.Now().UnixNano())

	subscriptions := NewSubscriptionEndpoint(os.Getenv("MOLLIE_API_KEY"))
	mandates := NewMandatesEndpoint(os.Getenv("MOLLIE_API_KEY"))

	mandat, _, err := mandates.Create(customer.ID, CreateMandateParams{
		Method:           resources.MandatemethodDirectdebit,
		ConsumerName:     customer.Name,
		ConsumerAccount:  "DE02120300000000202051",
		ConsumerBIC:      "BYLADEM1001",
		MandateReference: "GoMollieTest" + time.Now().Format(time.RFC3339),
		SignatureDate:    types.NewDate(time.Now()),
	})
	if err != nil {
		t.Fatal(err)
	}

	subscription, _, err := subscriptions.Create(customer.ID, CreateSubscriptionParams{
		Amount:      *types.NewAmount(100.0, types.CurrencyEUR),
		Times:       10,
		Interval:    types.NewInterval(1, types.IntervalperiodMonths),
		StartDate:   types.NewDate(time.Now()),
		Description: fmt.Sprintf("TEST-Subscription-%d", rand.Int()),
		MandateID:   mandat.ID,
	})
	if err != nil {
		t.Fatal(err)
	}

	return &subscription
}
