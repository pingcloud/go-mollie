package services

import (
	"fmt"
	"github.com/dghubble/sling"
	"bitbucket.org/pingcloud/go-mollie/resources"
	"bitbucket.org/pingcloud/go-mollie/types"
	"net/http"
)

// SubscriptionEnpoint - Interacts with the molli Subscription API
type SubscriptionEnpoint struct {
	client *sling.Sling
}

// NewSubscriptionEndpoint returns a new SubscriptionsEndpoint
func NewSubscriptionEndpoint(apiKey string) *SubscriptionEnpoint {
	return &SubscriptionEnpoint{
		client: NewClient(apiKey),
	}
}

// Create - https://docs.mollie.com/reference/v2/subscriptions-api/create-subscription
// With subscriptions, you can schedule recurring payments to take place at regular intervals.
//
// For example, by simply specifying an amount and an interval, you can create an endless subscription to charge a
// monthly fee, until you cancel the subscription.
//
// Or, you could use the times parameter to only charge a limited number of times, for example to split a big
// transaction in multiple parts.
//
// A few example usages:
//
// - `amount[currency]="EUR" amount[value]="5.00" interval="2 weeks"` Your consumer will be charged €5 once every two
// weeks.
//
// - amount[currency]="EUR" amount[value]="20.00" interval="1 day" times=5 Your consumer will be charged €20 every day,
// for five consecutive days.
//
// - amount[currency]="EUR" amount[value]="10.00" interval="1 month" startDate="2018-04-30" Your consumer will be
// charged €10 on the last day of each month, starting in April 2018.
func (e *SubscriptionEnpoint) Create(customerID string, params CreateSubscriptionParams) (resources.Subscription, *http.Response, error) {
	subscription := new(resources.Subscription)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Post(fmt.Sprintf("customers/%s/subscriptions", customerID)).BodyJSON(params).Receive(subscription, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return *subscription, resp, err
}

// Get - https://docs.mollie.com/reference/v2/subscriptions-api/get-subscription
// Retrieve a subscription by its ID and its customer’s ID.
func (e *SubscriptionEnpoint) Get(customerID, subscriptionID string) (resources.Subscription, *http.Response, error) {
	subscription := new(resources.Subscription)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Get(fmt.Sprintf("customers/%s/subscriptions/%s", customerID, subscriptionID)).Receive(subscription, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return *subscription, resp, err
}

// Cancel - https://docs.mollie.com/reference/v2/subscriptions-api/cancel-subscription
// A subscription can be canceled any time by calling `DELETE` on the resource endpoint.
func (e *SubscriptionEnpoint) Cancel(customerID, subscriptionID string) (resources.Subscription, *http.Response, error) {
	mollieError := new(resources.Error)
	subscription := new(resources.Subscription)
	resp, err := e.client.New().Delete(fmt.Sprintf("customers/%s/subscriptions/%s", customerID, subscriptionID)).Receive(subscription, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return *subscription, resp, err
}

// List - https://docs.mollie.com/reference/v2/subscriptions-api/list-subscriptions
// Retrieve all subscriptions of a customer.
func (e *SubscriptionEnpoint) List(customerID string, params ListParams) (*resources.SubscriptionList, *http.Response, error) {
	mollieError := new(resources.Error)
	subscriptions := new(resources.SubscriptionList)
	resp, err := e.client.New().Get(fmt.Sprintf("customers/%s/subscriptions", customerID)).QueryStruct(params).Receive(subscriptions, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return subscriptions, resp, err
}

// Update - https://docs.mollie.com/reference/v2/subscriptions-api/update-subscription
// Some fields of a subscription can be updated by calling PATCH on the resource endpoint. Each field is optional.
//
// You cannot update a canceled subscription.
func (e *SubscriptionEnpoint) Update(customerID, subscriptionID string, params UpdateSubscriptionParams) (resources.Subscription, *http.Response, error) {
	subscription := new(resources.Subscription)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Patch(fmt.Sprintf("customers/%s/subscriptions/%s", customerID, subscriptionID)).BodyJSON(params).Receive(subscription, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return *subscription, resp, err
}

// ListPayments - https://docs.mollie.com/reference/v2/subscriptions-api/list-subscriptions-payments
// Retrieve all payments of a specific subscriptions of a customer.
func (e *SubscriptionEnpoint) ListPayments(customerID string, subscriptionID string, params ListParams) (*resources.PaymentList, *http.Response, error) {
	mollieError := new(resources.Error)
	payments := new(resources.PaymentList)
	resp, err := e.client.New().Get(fmt.Sprintf("customers/%s/subscriptions/%s/payments", customerID, subscriptionID)).QueryStruct(params).Receive(payments, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return payments, resp, err
}

// CreateSubscriptionParams - https://docs.mollie.com/reference/v2/subscriptions-api/create-subscription#parameters
type CreateSubscriptionParams struct {
	// The amount that you want to charge, e.g. {"currency":"EUR", "value":"100.00"} if you would want to charge €100.00.
	Amount types.Amount `json:"amount"`

	// Total number of charges for the subscription to complete. Leave empty for an ongoing subscription.
	Times uint `json:"times,omitempty"`

	// Interval to wait between charges, for example 1 month or 14 days.
	Interval types.Interval `json:"interval"`

	// The start date of the subscription in YYYY-MM-DD format. This is the first day on which your customer will be
	// charged. When this parameter is not provided, the current date will be used instead.
	StartDate types.Date `json:"startDate,omitempty"`

	// A sescription unique per subscription . This will be included in the payment description along with the charge
	// date.
	Description string `json:"description"`

	// The payment method used for this subscription, either forced on creation or null if any of the customer’s valid
	// mandates may be used. Please note that this parameter can not set together with mandateId.
	Method resources.SubscriptionMethod `json:"method,omitempty"`

	// MandateID used for this subscription. Please note that this parameter can not set together with method.
	MandateID string `json:"mandateId,omitempty"`

	// Use this parameter to set a webhook URL for all subscription payments.
	WebhookURL string `json:"webhookUrl,omitempty"`

	// Provide any data you like, and we will save the data alongside the subscription. Whenever you fetch the
	// subscription with our API, we’ll also include the metadata. You can use up to 1kB of JSON.
	Metadata map[string]interface{} `json:"metadata,omitempty"`
}

// UpdateSubscriptionParams - https://docs.mollie.com/reference/v2/subscriptions-api/update-subscription#parameters
type UpdateSubscriptionParams struct {
	// The amount that you want to charge, e.g. {"currency":"EUR", "value":"100.00"} if you would want to change the
	// charge to €100.00.
	Amount types.Amount `url:"amount,omitempty"      json:"amount,omitempty"`

	// Total number of charges for the subscription to complete. Can not be less than number of times that subscription
	// has been charged.
	Times uint `url:"times,omitempty"       json:"times,omitempty"`

	// The start date of the subscription in YYYY-MM-DD format. This is the first day on which your customer will be
	// charged. Should always be in the future.
	StartDate types.Date `url:"startDate,omitempty"   json:"startDate,omitempty"`

	// A description unique per subscription . This will be included in the payment description along with the charge
	// date.
	Description string `url:"description,omitempty" json:"description,omitempty"`

	// Use this parameter to set a specific mandate for all subscription payments. If you set a method before, it will
	// be changed to null when setting this parameter.
	MandateID string `url:"mandateId,omitempty"   json:"mandateId,omitempty"`

	// Use this parameter to set a webhook URL for all subscription payments.
	WebhookURL string `url:"webhookUrl,omitempty"  json:"webhookUrl,omitempty"`

	// Provide any data you like, and we will save the data alongside the subscription. Whenever you fetch the
	// subscription with our API, we’ll also include the metadata. You can use up to 1kB of JSON.
	Metadata map[string]interface{} `url:"metadata,omitempty"    json:"metadata,omitempty"`
}
