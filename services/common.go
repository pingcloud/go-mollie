package services

import (
	"fmt"
	"runtime"

	"github.com/dghubble/sling"
)

// Version of the go-mollie SDK
const Version = "0.0.1"

// APIEndpoint of the mollie API
const APIEndpoint = "https://api.mollie.com"

// APIVersion is the Version of the mollie API
// https://docs.mollie.com/reference/v2/
const APIVersion = "v2"

// NewClient creates a MollieAPIClient with all its APIs
// apiKey should start with "test_" or "live_"
func NewClient(apiKey string) *sling.Sling {
	// Create mollie api client
	client := sling.New().Client(nil).Base(fmt.Sprintf("%s/%s/", APIEndpoint, APIVersion))

	// Add request headers
	client.Set("authorization", fmt.Sprintf("Bearer %s", apiKey))
	client.Set("user-agent", "go-mollie/"+Version+" Go/"+runtime.Version())

	return client
}

// ListParams - https://docs.mollie.com/guides/pagination
type ListParams struct {
	// From - Offset the result set to the object with this ID. The object with this ID is included in the result set as
	// well.
	From string `url:"from,omitempty"`

	// Limit - The number of objects to return (with a maximum of 250).
	Limit uint `url:"limit,omitempty"`
}
