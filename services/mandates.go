package services

import (
	"fmt"
	"github.com/dghubble/sling"
	"bitbucket.org/pingcloud/go-mollie/resources"
	"bitbucket.org/pingcloud/go-mollie/types"
	"net/http"
)

// MandatesEndpoint - Interact with the mollie Mandates API
type MandatesEndpoint struct {
	client *sling.Sling
}

// NewMandatesEndpoint returns a MandatesEndpoint
func NewMandatesEndpoint(apiKey string) *MandatesEndpoint {
	return &MandatesEndpoint{
		client: NewClient(apiKey),
	}
}

// Create - https://docs.mollie.com/reference/v2/mandates-api/create-mandate
// Create a mandate for a specific customer. Mandates allow you to charge a customer’s credit card or bank account
// recurrently.
//
// It is only possible to create mandates for IBANs with this endpoint. To create mandates for credit cards, have your
// customers perform a ‘first payment’ with their credit card.
func (e *MandatesEndpoint) Create(customerID string, request CreateMandateParams) (resources.Mandate, *http.Response, error) {
	mandate := new(resources.Mandate)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Post(fmt.Sprintf("customers/%s/mandates", customerID)).BodyJSON(request).Receive(mandate, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return *mandate, resp, err
}

// Get - https://docs.mollie.com/reference/v2/mandates-api/get-mandate
// Retrieve a mandate by its ID and its customer’s ID. The mandate will either contain IBAN or credit card details,
// depending on the type of mandate.
func (e *MandatesEndpoint) Get(customerID, mandateID string) (resources.Mandate, *http.Response, error) {
	mandate := new(resources.Mandate)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Get(fmt.Sprintf("customers/%s/mandates/%s", customerID, mandateID)).Receive(mandate, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return *mandate, resp, err
}

// List - https://docs.mollie.com/reference/v2/mandates-api/list-mandates
// Retrieve all mandates for the given customerId, ordered from newest to oldest.
//
// The results are paginated. See pagination for more information.
func (e *MandatesEndpoint) List(customerID string, params ListParams) (*resources.MandateList, *http.Response, error) {
	mollieError := new(resources.Error)
	mandates := new(resources.MandateList)
	resp, err := e.client.New().Get(fmt.Sprintf("customers/%s/mandates", customerID)).QueryStruct(params).Receive(mandates, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return mandates, resp, err
}

// Revoke - https://docs.mollie.com/reference/v2/mandates-api/revoke-mandate
// Revoke a customer’s mandate. You will no longer be able to charge the consumer’s bank account or credit card with
// this mandate and all connected subscriptions will be canceled.
func (e *MandatesEndpoint) Revoke(customerID string, mandateID string) (*http.Response, error) {
	mollieError := new(resources.Error)
	resp, err := e.client.New().Delete(fmt.Sprintf("customers/%s/mandates/%s", customerID, mandateID)).Receive(nil, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return resp, err
}

// CreateMandateParams - https://docs.mollie.com/reference/v2/mandates-api/create-mandate#parameters
type CreateMandateParams struct {
	// Payment method of the mandate.
	Method resources.MandateMethod `json:"method"`

	// The consumer’s name.
	ConsumerName string `json:"consumerName"`

	// The consumer’s IBAN.
	ConsumerAccount string `json:"consumerAccount"`

	// The consumer’s bank’s BIC.
	ConsumerBIC string `json:"consumerBic,omitempty"`

	// The date when the mandate was signed in YYYY-MM-DD format.
	SignatureDate types.Date `json:"signatureDate,omitempty"`

	// A custom mandate reference
	MandateReference string `json:"mandateReference,omitempty"`
}
