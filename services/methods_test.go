package services

import (
	"bitbucket.org/pingcloud/go-mollie/types"
	"os"
	"testing"
)

func TestMethodsEndpoint_Get(t *testing.T) {
	methods := NewMethodsEndpoint(os.Getenv("MOLLIE_API_KEY"))

	list, _, err := methods.List(ListMethodParams{
		Locale: types.LocaleDeDe,
		Amount: types.NewAmount(100, types.CurrencyEUR),
	})
	if err != nil {
		t.Fatal(err)
	}

	if len(list.Embedded.Methods) <= 0 {
		t.Fatal("No Paymentmethods received")
	}

	method, _, err := methods.Get(types.Method(list.Embedded.Methods[0].ID), GetMethodParams{
		Locale:   types.LocaleDeDe,
		Currency: types.CurrencyEUR,
		Include:  "pricing",
	})
	if err != nil {
		t.Fatal(err)
	}
	if method.ID != list.Embedded.Methods[0].ID {
		t.Fatalf("Expected ID to be %s, got %s", list.Embedded.Methods[0].ID, method.ID)
	}
}

func TestMethodsEndpoint_List(t *testing.T) {
	methods := NewMethodsEndpoint(os.Getenv("MOLLIE_API_KEY"))

	list, _, err := methods.List(ListMethodParams{
		Include:        "pricing",
		Locale:         types.LocaleDeDe,
		Amount:         types.NewAmount(100, types.CurrencyEUR),
		BillingCountry: "DE",
	})

	if err != nil {
		t.Fatal(err)
	}

	if list.Count != uint(len(list.Embedded.Methods)) {
		t.Fatalf("Expected %d mathods, got %d", list.Count, len(list.Embedded.Methods))
	}
}

func TestMethodsEndpoint_ListAll(t *testing.T) {
	methods := NewMethodsEndpoint(os.Getenv("MOLLIE_API_KEY"))

	list, _, err := methods.ListAll(ListAllMethodsParams{
		Locale:  types.LocaleDeDe,
		Include: "pricing",
		Amount:  types.NewAmount(100, types.CurrencyEUR),
	})

	if err != nil {
		t.Fatal(err)
	}

	if list.Count != uint(len(list.Embedded.Methods)) {
		t.Fatalf("Expected %d mathods, got %d", list.Count, len(list.Embedded.Methods))
	}

}

func TestMethodsEndpoint_Errors(t *testing.T) {
	methods := NewMethodsEndpoint(os.Getenv("MOLLIE_API_KEY"))

	_, _, err := methods.Get(types.Method("xxxxxxx"), GetMethodParams{
		Currency: types.Currency("XY1"),
		Locale:   types.LocaleDeDe,
	})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = methods.List(ListMethodParams{
		Locale: types.LocaleDeDe,
		Amount: types.NewAmount(-100, types.Currency("XY1")),
	})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = methods.ListAll(ListAllMethodsParams{
		Amount: types.NewAmount(-100, types.Currency("XY1")),
		Locale: types.LocaleDeDe,
	})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

}
