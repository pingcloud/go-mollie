package services

import (
	"fmt"
	"github.com/dghubble/sling"
	"bitbucket.org/pingcloud/go-mollie/resources"
	"bitbucket.org/pingcloud/go-mollie/types"
	"net/http"
)

// CustomersEnpoint - Interact with the mollie Customer API
type CustomersEnpoint struct {
	client *sling.Sling
}

// NewCustomerEndpoint return a new CustomersEndpoint
func NewCustomerEndpoint(apiKey string) *CustomersEnpoint {
	return &CustomersEnpoint{
		client: NewClient(apiKey),
	}
}

// Create - https://docs.mollie.com/reference/v2/customers-api/create-customer
// Creates a simple minimal representation of a customer in the Mollie API to use for the Mollie Checkout and Recurring
// features. These customers will appear in your Mollie Dashboard where you can manage their details, and also see their
// payments and subscriptions.
func (e *CustomersEnpoint) Create(params CreateCustomerParams) (*resources.Customer, *http.Response, error) {
	customer := new(resources.Customer)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Post("customers").BodyJSON(params).Receive(customer, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return customer, resp, err
}

// Delete - https://docs.mollie.com/reference/v2/customers-api/delete-customer
// Delete a customer. All mandates and subscriptions created for this customer will be canceled as well.
func (e *CustomersEnpoint) Delete(customerID string) (*http.Response, error) {

	mollieError := new(resources.Error)
	resp, err := e.client.New().Delete(fmt.Sprintf("customers/%s", customerID)).Receive(nil, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return resp, err
}

// Get - https://docs.mollie.com/reference/v2/customers-api/get-customer
// Retrieve a single customer by its ID.
func (e *CustomersEnpoint) Get(customerID string) (*resources.Customer, *http.Response, error) {
	customer := new(resources.Customer)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Get(fmt.Sprintf("customers/%s", customerID)).Receive(customer, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return customer, resp, err
}

// Update - https://docs.mollie.com/reference/v2/customers-api/update-customer
// Update an existing customer.
func (e *CustomersEnpoint) Update(customerID string, params CustomerUpdateParams) (*resources.Customer, *http.Response, error) {
	customer := new(resources.Customer)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Patch(fmt.Sprintf("customers/%s", customerID)).BodyJSON(params).Receive(customer, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return customer, resp, err
}

// List - https://docs.mollie.com/reference/v2/customers-api/list-customers
// Retrieve all customers created.
//
// The results are paginated. See pagination for more information.
func (e *CustomersEnpoint) List(params ListParams) (*resources.CustomerList, *http.Response, error) {
	mollieError := new(resources.Error)
	customers := new(resources.CustomerList)
	resp, err := e.client.New().Get("customers").QueryStruct(params).Receive(customers, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return customers, resp, err
}

// ListPayments - https://docs.mollie.com/reference/v2/customers-api/list-customer-payments
// Retrieve all payments linked to the customer.
func (e *CustomersEnpoint) ListPayments(customerID string, params ListParams) (*resources.PaymentList, *http.Response, error) {
	mollieError := new(resources.Error)
	payments := new(resources.PaymentList)
	resp, err := e.client.New().Get(fmt.Sprintf("customers/%s/payments", customerID)).QueryStruct(params).Receive(payments, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return payments, resp, err
}

// CreatePayment - https://docs.mollie.com/reference/v2/customers-api/create-customer-payment
// Creates a payment for the customer.
//
//Linking customers to payments enables a number of Mollie Checkout features, including:
//
// - Keeping track of payment preferences for your customers.
//
// - Enabling your customers to charge a previously used credit card with a single click.
//
// - Improved payment insights in your dashboard.
//
// - Recurring payments.
func (e *CustomersEnpoint) CreatePayment(customerID string, params CreatePaymentParams) (*resources.Payment, *http.Response, error) {
	payment := new(resources.Payment)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Post(fmt.Sprintf("customers/%s/payments", customerID)).BodyJSON(params).Receive(payment, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return payment, resp, err
}

// CreateCustomerParams - https://docs.mollie.com/reference/v2/customers-api/create-customer#parameters
type CreateCustomerParams struct {
	// The full name of the customer.
	Name string `json:"name,omitempty"`

	// The email address of the customer.
	Email string `json:"email,omitempty"`

	// Allows you to preset the language to be used in the hosted payment pages shown to the consumer. When this
	// parameter is not provided, the browser language will be used instead in the payment flow (which is usually more
	// accurate).
	Locale types.Locale `json:"locale,omitempty"`

	//Provide any data you like, and we will save the data alongside the customer. Whenever you fetch the customer with
	// our API, we’ll also include the metadata. You can use up to 1kB of JSON.
	Metadata map[string]interface{} `json:"metadata,omitempty"`
}

// CustomerUpdateParams - Update an existing customer.
type CustomerUpdateParams struct {
	// Name - The full name of the customer.
	Name string `json:"name,omitempty"`

	// Email - The email address of the customer.
	Email string `json:"email,omitempty"`

	// Locale - Allows you to preset the language to be used in the hosted payment pages shown to the consumer. When
	// this parameter is not provided, the browser language will be used instead in the payment flow (which is usually
	// more accurate).
	Locale types.Locale `json:"locale,omitempty"`

	// Metadata - Provide any data you like, and we will save the data alongside the customer. Whenever you fetch the
	// customer with our API, we’ll also include the metadata. You can use up to 1kB of JSON.
	Metadata map[string]interface{} `json:"metadata,omitempty"`
}
