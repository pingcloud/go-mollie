package services

import (
	"fmt"
	"github.com/dghubble/sling"
	"bitbucket.org/pingcloud/go-mollie/resources"
	"bitbucket.org/pingcloud/go-mollie/types"
	"net/http"
)

// OrdersEnpoint - https://docs.mollie.com/orders/overview
// The Orders API allows you to use Mollie for your order management. Pay after delivery payment methods, such as
// Klarna Pay later and Klarna Slice it require the Orders API and cannot be used with the Payments API.
type OrdersEnpoint struct {
	client *sling.Sling
}

// NewOrdersEndpoint returns a new OrdersEndpoint
func NewOrdersEndpoint(apiKey string) *OrdersEnpoint {
	return &OrdersEnpoint{
		client: NewClient(apiKey),
	}
}

// Create - https://docs.mollie.com/reference/v2/orders-api/create-order
// Using the Orders API is the preferred approach when integrating the Mollie API into e-commerce applications such as
// webshops. If you want to use pay after delivery methods such as Klarna Pay later, using the Orders API is mandatory.
//
// Creating an order will automatically create the required payment to allow your customer to pay for the order.
//
// Once you have created an order, you should redirect your customer to the URL in the _links.checkout property from the
// response.
//
// Note that when the payment fails, expires or is canceled, you can create a new payment using the Create order payment
// API. This is only possible for orders that have a created status.
func (e *OrdersEnpoint) Create(params *CreateOrderParams) (*resources.Order, *http.Response, error) {
	order := new(resources.Order)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Post("orders").BodyJSON(params).Receive(order, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return order, resp, err
}

// ListRefunds - https://docs.mollie.com/reference/v2/orders-api/list-order-refunds
// Retrieve all order refunds.
// The results are paginated.
func (e *OrdersEnpoint) ListRefunds(orderID string, params ListRefundParams) (*resources.RefundList, *http.Response, error) {
	refunds := new(resources.RefundList)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Get(fmt.Sprintf("orders/%s/refunds", orderID)).QueryStruct(params).Receive(refunds, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return refunds, resp, err
}

// Get - https://docs.mollie.com/reference/v2/orders-api/get-order
// Retrieve a single order by its ID.
func (e *OrdersEnpoint) Get(orderID string, params GetOrderParams) (*resources.Order, *http.Response, error) {
	order := new(resources.Order)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Get(fmt.Sprintf("orders/%s", orderID)).QueryStruct(params).Receive(order, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return order, resp, err
}

// Cancel - https://docs.mollie.com/reference/v2/orders-api/cancel-order
// The order can only be canceled while the order’s status field is either created, authorized or shipping [1].
//
// 1. In case of created, all order lines will be canceled and the new order status will be canceled.
//
// 2. In case of authorized, the authorization will be released, all order lines will be canceled and the new order \
// status will be canceled.
//
// 3. In case of shipping, any order lines that are still authorized will be canceled and order lines that are shipping
// will be completed. The new order status will be completed.
//
// For more information about the status transitions please check our order status changes guide.
func (e *OrdersEnpoint) Cancel(orderID string) (*resources.Order, *http.Response, error) {
	order := new(resources.Order)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Delete(fmt.Sprintf("orders/%s", orderID)).Receive(order, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return order, resp, err
}

// Update - https://docs.mollie.com/reference/v2/orders-api/update-order
// This endpoint can be used to update the billing and/or shipping address of an order.
// When updating an order that uses a pay after delivery method such as Klarna Pay later, Klarna may decline the
// requested changes, resulting in an error response from the Mollie API. The order remains intact, though the requested
// changes are not persisted.
func (e *OrdersEnpoint) Update(orderID string, params UpdateOrderParams) (*resources.Order, *http.Response, error) {
	order := new(resources.Order)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Patch(fmt.Sprintf("orders/%s", orderID)).BodyJSON(params).Receive(order, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return order, resp, err
}

// List - https://docs.mollie.com/reference/v2/orders-api/list-orders
// Retrieve all orders.
// The results are paginated. See pagination for more information.
func (e *OrdersEnpoint) List(params ListParams) (*resources.OrderList, *http.Response, error) {
	mollieError := new(resources.Error)
	orders := new(resources.OrderList)
	resp, err := e.client.New().Get("orders").QueryStruct(params).Receive(orders, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return orders, resp, err
}

// UpdateOrderLine - https://docs.mollie.com/reference/v2/orders-api/update-orderline
// This endpoint can be used to update an order line. Only the lines that belong to an order with status created,
// pending or authorized can be updated.
//
// Use cases for this endpoint could be updating the name, productUrl and imageUrl for a certain order line because your
// customer wants to swap the item for a different variant, for example exchanging a blue skirt for the same skirt in
// red.
//
// Or update the quantity, unitPrice, discountAmount, totalAmount, vatAmount and vatRate if you want to substitute a
// product for an entirely different one.
//
// Alternatively, you can also (partially) cancel order lines instead of updating the quantity.
//
// When updating an order line that uses a pay after delivery method such as Klarna Pay later, Klarna may decline the
// requested changes, resulting in an error response from the Mollie API. The order remains intact, though the requested
// changes are not persisted.
func (e *OrdersEnpoint) UpdateOrderLine(orderID string, orderlineID string, params UpdateOrderLineParams) (*resources.Order, *http.Response, error) {
	order := new(resources.Order)
	mollieError := new(resources.Error)
	resp, err := e.client.New().Patch(fmt.Sprintf("orders/%s/lines/%s", orderID, orderlineID)).BodyJSON(params).Receive(order, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return order, resp, err
}

// CancelOrderLines - https://docs.mollie.com/reference/v2/orders-api/cancel-order-lines
// This endpoint can be used to cancel one or more order lines that were previously authorized using a pay after
// delivery payment method. Use the Cancel Order API if you want to cancel the entire order or the remainder of the
// order.
//
// Canceling or partially canceling an order line will immediately release the authorization held for that amount.
// Your customer will be able to see the updated order in his portal / app. Any canceled lines will be removed from
// the customer’s point of view, but will remain visible in the Mollie Dashboard.
//
// You should cancel an order line if you don’t intend to (fully) ship it.
//
// An order line can only be canceled while its status field is either authorized or shipping. If you cancel an
// authorized order line, the new order line status will be canceled. Canceling a shipping order line will result in a
// completed order line status.
//
// If the order line is paid or already completed, you should create a refund using the Create Order Refund API instead.
//
// For more information about the status transitions please check our order status changes guide.
func (e *OrdersEnpoint) CancelOrderLines(orderID string, params CancelOrderLineParams) (*http.Response, error) {
	mollieError := new(resources.Error)
	resp, err := e.client.New().Delete(fmt.Sprintf("orders/%s/lines", orderID)).BodyJSON(params).Receive(nil, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return resp, err
}

// CreatePayment - https://docs.mollie.com/reference/v2/orders-api/create-order-payment
// An order has an automatically created payment that your customer can use to pay for the order. When the payment
// expires you can create a new payment for the order using this endpoint.
//
// A new payment can only be created while the status of the order is created, and when the status of the existing
// payment is either expired, canceled or failed.
//
// Note that order details (for example amount or webhookUrl) can not be changed using this endpoint.
func (e *OrdersEnpoint) CreatePayment(orderID string, params PaymentSpecificParameters) (*resources.Payment, *http.Response, error) {
	mollieError := new(resources.Error)
	payment := new(resources.Payment)
	resp, err := e.client.New().Get(fmt.Sprintf("orders/%s/payments", orderID)).BodyJSON(params).Receive(payment, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return payment, resp, mollieError
}

// CreateRefund - https://docs.mollie.com/reference/v2/orders-api/create-order-refund
// When using the Orders API, refunds should be made against the order. When using pay after delivery payment methods
// such as Klarna Pay later and Klarna Slice it, this ensures that your customer will receive credit invoices with the
// correct product information on them and have a great experience.
//
// However, if you want to refund arbitrary amounts you can use the Create Payment Refund API for Pay later and Slice
// it.
//
// If an order line is still in the authorized status, it cannot be refunded. You should cancel it instead. Order lines
// that are paid, shipping or completed can be refunded.
//
// For more details on how refunds work, see Create Payment Refund API.
func (e *OrdersEnpoint) CreateRefund(orderID string, params CreateRefundParams) (*resources.Refund, *http.Response, error) {
	mollieError := new(resources.Error)
	refund := new(resources.Refund)
	resp, err := e.client.New().Get(fmt.Sprintf("orders/%s/refunds", orderID)).BodyJSON(params).Receive(refund, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return refund, resp, mollieError
}

// CreateRefundParams - https://docs.mollie.com/reference/v2/orders-api/create-order-refund#parameters
type CreateRefundParams struct {
	// Lines - An array of objects containing the order line details you want to create a refund for. If you send an
	// empty array, the entire order will be refunded.
	Lines []struct {
		// ID - The API resource token of the order line, for example: odl_jp31jz.
		ID string `json:"id"`
		// Quantity - The number of items that should be refunded for this order line. When this parameter is omitted,
		// the whole order line will be refunded.
		// Must be less than the number of items already refunded for this order line.
		Quantity uint `json:"quantity,omitempty"`
		// Amount - The amount that you want to refund. In almost all cases, Mollie can determine the amount
		// automatically.
		// The amount is required only if you are partially refunding an order line which has a non-zero discountAmount.
		// The amount you can refund depends on various properties of the order line and the create order refund
		// request. The maximum that can be refunded is unit price x quantity to ship.
		// The minimum amount depends on the discount applied to the line, the quantity already refunded or shipped, the
		// amounts already refunded or shipped and the quantity you want to refund.
		// If you do not send an amount, Mollie will determine the amount automatically or respond with an error if the
		// amount cannot be determined automatically. The error will contain the extra.minimumAmount and
		// extra.maximumAmount properties that allow you pick the right amount.
		Amount *types.Amount `json:"amount,omitempty"`
	} `json:"lines"`
	// Description - The description of the refund you are creating. This will be shown to the consumer on their card or
	// bank statement when possible. Max. 140 characters.
	Description string `json:"description,omitempty"`
	// Metadata - Provide any data you like, for example a string or a JSON object. We will save the data alongside the
	// refund. Whenever you fetch the refund with our API, we’ll also include the metadata. You can use up to
	// approximately 1kB.
	Metadata map[string]interface{} `json:"metadata,omitempty"`
}

// CancelOrderLineParams - https://docs.mollie.com/reference/v2/orders-api/cancel-order-lines#parameters
type CancelOrderLineParams struct {
	Lines []struct {
		ID       string        `json:"id"`
		Quantity uint          `json:"quantity,omitempty"`
		Amount   *types.Amount `json:"amount,omitempty"`
	} `json:"lines"`
}

// UpdateOrderLineParams - https://docs.mollie.com/reference/v2/orders-api/update-orderline#parameters
type UpdateOrderLineParams struct {
	Name string `json:"name,omitempty"`

	ImageURL string `json:"imageUrl,omitempty"`

	ProductURL string `json:"productUrl,omitempty"`

	Quantity uint `json:"quantity,omitempty"`

	UnitPrice *types.Amount `json:"unitPrice,omitempty"`

	DiscountAmount *types.Amount `json:"discountAmount,omitempty"`

	TotalAmount *types.Amount `json:"totalAmount,omitempty"`

	VatAmount *types.Amount `json:"vatAmount,omitempty"`

	VatRate resources.VatRate `json:"vatRate,omitempty"`
}

// GetOrderParams - https://docs.mollie.com/reference/v2/orders-api/get-order#embedding-of-related-resources
type GetOrderParams struct {
	Embed string `json:"embed,omitempty" url:"embed,omitempty"`
}

// UpdateOrderParams - https://docs.mollie.com/reference/v2/orders-api/update-order#parameters
type UpdateOrderParams struct {
	BillingAddress *types.OrderAddress `json:"billingAddress,omitempty"`

	ShippingAddress *types.OrderAddress `json:"shippingAddress,omitempty"`

	OrderNumber string `json:"orderNumber,omitempty"`
}

// CreateOrderLineParams - https://docs.mollie.com/reference/v2/orders-api/create-order#order-line-details
type CreateOrderLineParams struct {
	Type resources.OrderLineType `json:"type"`

	Name string `json:"name"`

	Quantity uint `json:"quantity"`

	UnitPrice types.Amount `json:"unitPrice"`

	DiscountAmount *types.Amount `json:"discountAmount,omitempty"`

	TotalAmount types.Amount `json:"totalAmount"`

	VatRate resources.VatRate `json:"vatRate"`

	VatAmount types.Amount `json:"vatAmount"`

	SKU string `json:"sku,omitempty"`

	ImageURL string `json:"imageUrl,omitempty"`

	ProductURL string `json:"productUrl,omitempty"`

	Metadata map[string]interface{} `json:"metadata,omitempty"`
}

// CreateOrderParams - https://docs.mollie.com/reference/v2/orders-api/create-order#parameters
type CreateOrderParams struct {
	// Amount - The total amount of the order, including VAT and discounts. This is the amount that will be charged to
	// your customer.
	//
	// For example: {"currency":"EUR", "value":"100.00"} if the total order amount is €100.00.
	Amount types.Amount `json:"amount"`

	// OrderNumber - The order number. For example, 16738.
	//
	// We recommend that each order should have a unique order number.
	OrderNumber string `json:"orderNumber"`

	// Lines - The lines in the order. Each line contains details such as a description of the item ordered, its price
	// et cetera. See Order line details for the exact details on the lines.
	Lines []CreateOrderLineParams `json:"lines"`

	// BillingAddress - The billing person and address for the order. See Order address details for the exact fields
	// needed.
	BillingAddress *types.OrderAddress `json:"billingAddress"`

	// ShippingAddress - The shipping address for the order. See Order address details for the exact fields needed. If
	// omitted, it is assumed to be identical to the billingAddress.
	ShippingAddress *types.OrderAddress `json:"shippingAddress,omitempty"`

	// ConsumerDateOfBirth - The date of birth of your customer. Some payment methods need this value and if you have
	// it, you should send it so that your customer does not have to enter it again later in the checkout process.
	ConsumerDateOfBirth types.Date `json:"consumerDateOfBirth,omitempty"`

	RedirectURL string `json:"redirectUrl"`

	WebhookURL string `json:"webhookUrl,omitempty"`

	Locale types.Locale `json:"locale"`

	Method types.Method `json:"method,omitempty"`

	Payment *PaymentSpecificParameters `json:"payment,omitempty"`

	Metadata map[string]interface{} `json:"metadata,omitempty"`
}

// PaymentSpecificParameters - https://docs.mollie.com/reference/v2/orders-api/create-order#payment-specific-parameters
// Creating an order will automatically create a payment that your customer can use to pay for the order. Creation of
// the payment can be controlled using the method and payment parameters.
//
// The optional method parameter ensures that order can be paid for using a specific payment method. If the parameter is
// omitted, your customer will be presented with a method selection screen and can check out using any of the available
// payment methods on your website profile.
//
// Optional parameters may be available for that payment method. If no method is specified, you can still send the
// optional parameters and we will apply them when your customer selects the relevant payment method.
//
// All payment specific parameters must be passed in the payment object. The following payment specific parameters can
// be passed during order creation:
type PaymentSpecificParameters struct {
	ConsumerAccount string `json:"consumerAccount,omitempty"`

	CustomerID string `json:"customerId,omitempty"`

	CustomerReference string `json:"customerReference,omitempty"`

	Issuer string `json:"issuer,omitempty"`

	MandateID string `json:"mandateId,omitempty"`

	SequenceType types.SequenceType `json:"sequenceType,omitempty"`

	VoucherNumber string `json:"voucherNumber,omitempty"`

	VoucherPin string `json:"voucherPin,omitempty"`

	WebhookURL string `json:"webhookUrl,omitempty"`
}

// ListRefundParams - https://docs.mollie.com/reference/v2/orders-api/list-order-refunds#parameters
type ListRefundParams struct {
	// ListParams - https://docs.mollie.com/reference/v2/orders-api/list-order-refunds#parameters
	ListParams `json:",inline,omitempty" url:",inline,omitempty"`

	// Embed - https://docs.mollie.com/reference/v2/orders-api/list-order-refunds#embedding-of-related-resources
	// This endpoint allows for embedding additional information by appending the following values via the embed query
	// string parameter.
	Embed string `json:"embed,omitempty" url:"embed,omitempty"`
}
