package services

import (
	"fmt"
	"github.com/dghubble/sling"
	"bitbucket.org/pingcloud/go-mollie/resources"
	"bitbucket.org/pingcloud/go-mollie/types"
	"net/http"
)

// MethodsEndpoint interacts with teh mollie Methods API
type MethodsEndpoint struct {
	client *sling.Sling
}

// NewMethodsEndpoint returns a new MethodsEndpoint
func NewMethodsEndpoint(apiKey string) *MethodsEndpoint {
	return &MethodsEndpoint{
		client: NewClient(apiKey),
	}
}

// List - https://docs.mollie.com/reference/v2/methods-api/list-methods
// Retrieve all enabled payment methods. The results are not paginated.
//
// For test mode, payment methods are returned that are enabled in the Dashboard (or the activation is pending).
// For live mode, payment methods are returned that have been activated on your account and have been enabled in the
// Dashboard.
// New payment methods can be activated via the Enable payment method endpoint in the Profiles API.
//
// When using the first sequence type, methods will be returned if they can be used as a first payment in a recurring
// sequence and if they are enabled in the Dashboard.
//
// When using the recurring sequence type, payment methods that can be used for recurring payments or subscriptions will
// be returned. Enabling / disabling methods in the dashboard does not affect how they can be used for recurring
// payments.
func (e *MethodsEndpoint) List(params ListMethodParams) (*resources.MethodList, *http.Response, error) {
	mollieError := new(resources.Error)
	methods := new(resources.MethodList)
	resp, err := e.client.New().Get("methods").QueryStruct(params).Receive(methods, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return methods, resp, err
}

// ListAll - https://docs.mollie.com/reference/v2/methods-api/list-all-methods
// Retrieve all payment methods that Mollie offers and can be activated by the Organization. The results are not
// paginated. New payment methods can be activated via the Enable payment method endpoint in the Profiles API.
func (e *MethodsEndpoint) ListAll(params ListAllMethodsParams) (*resources.MethodList, *http.Response, error) {
	mollieError := new(resources.Error)
	methods := new(resources.MethodList)
	resp, err := e.client.New().Get("methods/all").QueryStruct(params).Receive(methods, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return methods, resp, err
}

// Get - https://docs.mollie.com/reference/v2/methods-api/get-method
// Retrieve a single method by its ID. Note that if a method is not available on the website profile a status
// 404 Not found is returned. When the method is not enabled, a status 403 Forbidden is returned. You can enable
// payments methods via the Enable payment method endpoint in the Profiles API.
//
// If you do not know the method’s ID, you can use the methods list endpoint to retrieve all payment methods that are
// available.
func (e *MethodsEndpoint) Get(name types.Method, params GetMethodParams) (resources.Method, *http.Response, error) {
	mollieError := new(resources.Error)
	method := new(resources.Method)
	resp, err := e.client.New().Get(fmt.Sprintf("methods/%s", name)).QueryStruct(params).Receive(method, mollieError)
	if err == nil && mollieError.Title != "" {
		err = mollieError
	}
	return *method, resp, err
}

// ListMethodParams - https://docs.mollie.com/reference/v2/methods-api/list-methods#parameters
type ListMethodParams struct {
	// Passing first will only show payment methods eligible for making a first payment. Passing recurring shows payment
	// methods which can be used to automatically charge your customer’s account when authorization has been given. Set
	// to oneoff by default, which indicates the payment method is available for a regular non-recurring payment.
	SequenceType types.SequenceType `url:"sequenceType,omitempty"`

	// Passing a locale will sort the payment methods in the preferred order for the country, and translate the payment
	// method names in the corresponding language.
	Locale types.Locale `url:"locale,omitempty"`

	// An object containing value and currency. Only payment methods that support the amount and currency are returned.
	Amount *types.Amount `url:"amount,omitempty"`

	// Use the resource parameter to indicate if you will use the result with the Create Order or Create Payment API.
	//
	// For example: when passing orders the result will include payment methods that can only be used in conjunction
	// with orders, such as Klarna Pay later. Default behaviour is returning all available payment methods for payments.
	Resource types.MethodResource `url:"resource,omitempty"`

	// The billing country of your customer in ISO 3166-1 alpha-2 format. This parameter can be used to check whether
	// your customer is eligible for certain payment methods, for example Klarna Slice it.
	BillingCountry string `url:"billingCountry,omitempty"` // TODO: Type!

	// This endpoint allows you to include additional information by appending the following values via the include
	// querystring parameter.
	//
	// - `issuers` Include issuers available for the payment method (e.g. for iDEAL, KBC/CBC payment button or gift
	// cards).
	//
	// - `pricing` Include pricing for each payment method.
	Include string `url:"include,omitempty"`
}

// ListAllMethodsParams - https://docs.mollie.com/reference/v2/methods-api/list-all-methods
type ListAllMethodsParams struct {
	// Passing a locale will translate the payment method names in the corresponding language.
	Locale types.Locale `url:"locale,omitempty"`

	// An object containing value and currency. Only payment methods that support the amount and currency are returned.
	Amount *types.Amount `url:"amount,omitempty"`

	// This endpoint allows you to include additional information by appending the following values via the include
	// querystring parameter.
	//
	// - `issuers` Include issuers available for the payment method (e.g. for iDEAL, KBC/CBC payment button or gift
	// cards).
	//
	// - `pricing` Include pricing for each payment method.
	Include string `url:"include,omitempty"`
}

// GetMethodParams - https://docs.mollie.com/reference/v2/methods-api/get-method#parameters
type GetMethodParams struct {
	// Passing a locale will translate the payment method name in the corresponding language.
	Locale types.Locale `url:"locale,omitempty"`

	// The currency to receiving the minimumAmount and maximumAmount in. We will return an error when the currency is
	// not supported by the payment method.
	Currency types.Currency `url:"currency,omitempty"`

	// This endpoint allows you to include additional information by appending the following values via the include
	// querystring parameter.
	//
	// - `issuers` Include issuers available for the payment method (e.g. for iDEAL, KBC/CBC payment button or gift
	// cards).
	//
	// - `pricing` Include pricing for each payment method.
	Include string `url:"include,omitempty"`
}
