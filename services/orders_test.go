package services

import (
	"github.com/icrowley/fake"
	"bitbucket.org/pingcloud/go-mollie/resources"
	"bitbucket.org/pingcloud/go-mollie/types"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestOrdersEnpoint_Create(t *testing.T) {
	orders := NewOrdersEndpoint(os.Getenv("MOLLIE_API_KEY"))
	order := createOrder(nil, t)
	defer func() {
		orders.Cancel(order.ID)
	}()
	if strings.Index(order.ID, "ord_") != 0 {
		t.Errorf("Order ID should start with 'ord_', but is '%s'", order.ID)
	}

}

func TestOrdersEnpoint_Update(t *testing.T) {
	orders := NewOrdersEndpoint(os.Getenv("MOLLIE_API_KEY"))
	order1 := createOrder(nil, t)
	rand.Seed(time.Now().UnixNano())
	newOrderNumber := "NEWORDER+" + strconv.Itoa(rand.Int())

	order2, _, err := orders.Update(order1.ID, UpdateOrderParams{
		OrderNumber: newOrderNumber,
	})
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		orders.Cancel(order1.ID)
	}()
	if order2.OrderNumber != newOrderNumber {
		t.Fatalf("Expected orderNumber to be %s, but got %s", newOrderNumber, order2.OrderNumber)
	}
}

func TestOrdersEnpoint_Get(t *testing.T) {
	orders := NewOrdersEndpoint(os.Getenv("MOLLIE_API_KEY"))

	customer, err := createCustomer()
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		customers := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))
		customers.Delete(customer.ID)
	}()

	order1 := createOrder(customer, t)
	defer func() {
		orders.Cancel(order1.ID)
	}()

	order2, _, err := orders.Get(order1.ID, GetOrderParams{"payments,refunds"})
	if err != nil {
		t.Fatal(err)
	}

	if order1.ID != order2.ID || order2.ID == "" {
		t.Fatalf("Expected OrderID to be %s, but got: %s", order1.ID, order2.ID)
	}

}

func TestOrdersEnpoint_List(t *testing.T) {
	orders := NewOrdersEndpoint(os.Getenv("MOLLIE_API_KEY"))

	order1 := createOrder(nil, t)
	defer func() {
		orders.Cancel(order1.ID)
	}()
	order2 := createOrder(nil, t)
	defer func() {
		orders.Cancel(order2.ID)
	}()

	list, _, err := orders.List(ListParams{Limit: 2})
	if err != nil {
		t.Fatal(err)
	}

	if list.Count != 2 || list.Count != uint(len(list.Embedded.Orders)) {
		t.Fatalf("Expected 2 orders, got %d", len(list.Embedded.Orders))
	}

}

func TestOrdersEnpoint_Cancel(t *testing.T) {
	orders := NewOrdersEndpoint(os.Getenv("MOLLIE_API_KEY"))

	order1 := createOrder(nil, t)

	order2, _, err := orders.Cancel(order1.ID)
	if err != nil {
		t.Fatal(err)
	}

	if order2.Status != resources.OrderStatusCanceled {
		t.Fatalf("Expected Status to be %s, but got %s", resources.OrderStatusCanceled, order2.Status)
	}
}

func TestOrdersEnpoint_Errors(t *testing.T) {
	orders := NewOrdersEndpoint(os.Getenv("MOLLIE_API_KEY"))

	_, _, err := orders.Create(&CreateOrderParams{})
	if err == nil {
		t.Fatal("Expected an error, but got none")
	}

	_, _, err = orders.List(ListParams{Limit: 0, From: "xxx_123456"})
	if err == nil {
		t.Fatal("Expected an error, but got none")
	}

	_, _, err = orders.Get("xx_123456", GetOrderParams{})
	if err == nil {
		t.Fatal("Expected an error, but got none")
	}

	_, _, err = orders.Cancel("xxx_123456")
	if err == nil {
		t.Fatal("Expected an error, but got none")
	}
}

func createOrder(customer *resources.Customer, t *testing.T) *resources.Order {

	rand.Seed(time.Now().UnixNano())

	orders := NewOrdersEndpoint(os.Getenv("MOLLIE_API_KEY"))

	vatAmount := float32(99.0 * (19.0 / (100.0 + 19.0)))

	orderRequest := &CreateOrderParams{
		Amount:      *types.NewAmount(99, types.CurrencyEUR),
		OrderNumber: "TestOrder-" + strconv.Itoa(rand.Int()),
		Lines: []CreateOrderLineParams{
			{
				Type:        resources.OrderLineTypeDigital,
				Name:        "Test-Product",
				Quantity:    3,
				UnitPrice:   *types.NewAmount(33, types.CurrencyEUR),
				TotalAmount: *types.NewAmount(99, types.CurrencyEUR),
				VatRate:     resources.NewVatRate(19),
				VatAmount:   *types.NewAmount(vatAmount, types.CurrencyEUR),
			},
		},
		BillingAddress: &types.OrderAddress{
			Email:      fake.UserName() + "@mollie.webstollen.de",
			FamilyName: fake.LastName(),
			GivenName:  fake.FirstName(),
			Address: types.Address{
				City:            fake.City(),
				Country:         "DE",
				PostalCode:      strconv.Itoa(rand.Intn(99999-10000) + 10000),
				StreetAndNumber: fake.StreetAddress(),
			},
		},
		Locale:      types.LocaleDeDe,
		RedirectURL: "https://www.webstollen.de/test",
	}

	if customer != nil {
		orderRequest.Payment = &PaymentSpecificParameters{
			CustomerID: customer.ID,
		}
	}

	order, _, err := orders.Create(orderRequest)

	if err != nil {
		t.Fatal(err)
	}
	return order
}
