package services

import (
	"github.com/icrowley/fake"
	_ "bitbucket.org/pingcloud/go-mollie/resources"
	"bitbucket.org/pingcloud/go-mollie/types"
	"os"
	"strings"
	"testing"
)

func TestCustomersEnpoint_Create(t *testing.T) {

	client := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))
	customer, err := createCustomer()
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		_, err = client.Delete(customer.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()

	if strings.Index(customer.ID, "cst_") != 0 {
		t.Fatalf("Customer ID should start with 'cst_', but is '%s'", customer.ID)
	}

}

func TestCustomersEnpoint_Get(t *testing.T) {
	client := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))
	customer1, err := createCustomer()
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		_, err = client.Delete(customer1.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()

	customer2, _, err := client.Get(customer1.ID)
	if err != nil {
		t.Fatal(err)
	}

	if customer2.ID != "" && customer1.ID != customer2.ID {
		t.Fatalf("Expected Customer ID '%s', but got '%s'", customer1.ID, customer2.ID)
	}

}

func TestCustomersEnpoint_List(t *testing.T) {

	client := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))

	cus1, err := createCustomer()
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		_, err = client.Delete(cus1.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()
	cus2, err := createCustomer()
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		_, err = client.Delete(cus2.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()

	customers, _, err := client.List(ListParams{})
	if err != nil {
		t.Fatal(err)
	}
	if customers.Count < 2 {
		t.Fatalf("Expected at least 2 Customers, got: %d", customers.Count)
	}

}

func TestCustomersEnpoint_Delete(t *testing.T) {

	client := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))

	customer, err := createCustomer()
	if err != nil {
		t.Fatal(err)
	}

	_, err = client.Delete(customer.ID)
	if err != nil {
		t.Fatal(err)
	}

	_, err = client.Delete(customer.ID)
	if err == nil {
		t.Fatal("Repeated deletion should result in an error, it didn't!")
	}

}

func TestCustomersEnpoint_Update(t *testing.T) {

	client := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))

	customer1, err := createCustomer()
	if err != nil {
		t.Error(err)
	}
	defer func() {
		_, err = client.Delete(customer1.ID)
		if err != nil {
			t.Fatal(err)
		}
	}()

	mail := fake.UserName() + "@mollie.webstollen.de"
	name := fake.FirstName() + " " + fake.LastName()

	customer2, _, err := client.Update(customer1.ID, CustomerUpdateParams{
		Name:  name,
		Email: mail,
	})
	if err != nil {
		t.Fatal(err)
	}

	if customer2.Name != name || customer2.Email != mail {
		t.Fatalf("Expected %s (%s), got %s (%s)", name, mail, customer2.Name, customer2.Email)
	}

}

func TestCustomersEnpoint_Errors(t *testing.T) {
	customers := NewCustomerEndpoint(os.Getenv("MOLLIE_API_KEY"))

	_, _, err := customers.Create(CreateCustomerParams{Locale: types.Locale("xx_YY")})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = customers.Get("xxx_12345")
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = customers.List(ListParams{From: "xxx_123456", Limit: 0})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, _, err = customers.Update("xxx_123456", CustomerUpdateParams{})
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

	_, err = customers.Delete("xxx_12312412")
	if err == nil {
		t.Fatal("Expected an error, but got none!")
	}

}
