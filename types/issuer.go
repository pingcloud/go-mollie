package types

// GiftCardIssuer - https://docs.mollie.com/reference/v2/payments-api/create-payment#gift-cards
type GiftCardIssuer string

const (
	// IssuerFashioncheque for $fashioncheque
	IssuerFashioncheque GiftCardIssuer = "fashioncheque"
	// IssuerNationalebioscoopbon for $nationalebioscoopbon
	IssuerNationalebioscoopbon GiftCardIssuer = "nationalebioscoopbon"
	// IssuerNationaleentertainmentcard for $nationaleentertainmentcard
	IssuerNationaleentertainmentcard GiftCardIssuer = "nationaleentertainmentcard"
	// IssuerKunstencultuurcadeaukaart for $kunstencultuurcadeaukaart
	IssuerKunstencultuurcadeaukaart GiftCardIssuer = "kunstencultuurcadeaukaart"
	// IssuerPodiumcadeaukaart for $podiumcadeaukaart
	IssuerPodiumcadeaukaart GiftCardIssuer = "podiumcadeaukaart"
	// IssuerVvvgiftcard for $vvvgiftcard
	IssuerVvvgiftcard GiftCardIssuer = "vvvgiftcard"
	// IssuerVvvdinercheque for $vvvdinercheque
	IssuerVvvdinercheque GiftCardIssuer = "vvvdinercheque"
	// IssuerVvvlekkerweg for $vvvlekkerweg
	IssuerVvvlekkerweg GiftCardIssuer = "vvvlekkerweg"
	// IssuerWebshopgiftcard for $webshopgiftcard
	IssuerWebshopgiftcard GiftCardIssuer = "webshopgiftcard"
	// IssuerYourgift for $yourgift
	IssuerYourgift GiftCardIssuer = "yourgift"
	// IssuerTravelcheq for $travelcheq
	IssuerTravelcheq GiftCardIssuer = "travelcheq"
	// IssuerNatinalegolfbon for $nationalegolfbon
	IssuerNatinalegolfbon GiftCardIssuer = "nationalegolfbon"
	// IssuerSportenfitcadeau for $sportenfitcadeau
	IssuerSportenfitcadeau GiftCardIssuer = "sportenfitcadeau"
)

// KBCCBCIssuer - https://docs.mollie.com/reference/v2/payments-api/create-payment#kbc-cbc-payment-button
type KBCCBCIssuer string

const (
	// IssuerKBC for "kbc"
	IssuerKBC KBCCBCIssuer = "kbc"
	// IssuerCBC for "cbc"
	IssuerCBC KBCCBCIssuer = "cbc"
)
