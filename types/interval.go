package types

import "fmt"

// Interval string
type Interval string

// IntervalPeriod type
type IntervalPeriod string

const (
	// IntervalperiodMonths for monthly payments
	IntervalperiodMonths IntervalPeriod = "%d months"
	// IntervalperiodWeeks for weekly payments
	IntervalperiodWeeks IntervalPeriod = "%d weeks"
	// IntervalperiodDays for daily payments
	IntervalperiodDays IntervalPeriod = "%d days"
)

// NewInterval returns a valid Interval string
func NewInterval(every uint, period IntervalPeriod) Interval {
	return Interval(fmt.Sprintf(string(period), every))
}
