package types

// Address - https://docs.mollie.com/guides/common-data-types#address-object
// In the v2 endpoints, an address object is always represented as follows.
type Address struct {
	// StreetAndNumber - The street and street number of the address.
	StreetAndNumber string `json:"streetAndNumber,omitempty"`
	// StreetAdditional - Any additional addressing details, for example an apartment number.
	StreetAdditional string `json:"street_additional,omitempty"`
	// PostalCode - The postal code of the address.
	PostalCode string `json:"postalCode,omitempty"`
	// City - The city of the address.
	City string `json:"city,omitempty"`
	// Region - The region of the address.
	Region string `json:"region,omitempty"`
	// Country - The country of the address in ISO 3166-1 alpha-2 format.
	Country string `json:"country,omitempty"` // TODO : TYPE
}

// OrderAddress - https://docs.mollie.com/reference/v2/orders-api/create-order#order-address-details
// In the Orders API, the address objects identify both the address and the person the order is billed or shipped to.
// At least a valid address must be passed as well as fields identifying the person.
type OrderAddress struct {
	// OrganizatiopnName - The person’s organization, if applicable.
	OrganizationName string `json:"organizationName,omitempty"`
	// Title - The title of the person, for example Mr. or Mrs..
	Title string `json:"title,omitempty"`
	// GiveName - The given name (first name) of the person.
	GivenName string `json:"givenName"`
	// FamilyName - The family name (surname) of the person.
	FamilyName string `json:"familyName"`
	// Email - The email address of the person.
	Email string `json:"email"`
	// Phone - The phone number of the person. Some payment methods require this information. If you have it, you
	// should pass it so that your customer does not have to enter it again in the checkout. Must be in the E.164
	// format. For example +31208202070.
	Phone string `json:"phone,omitempty"`
	// Address - The other address fields. Please refer to the documentation of the address object for more information
	// on which inputs are accepted inputs.
	Address `json:",inline"`
}
