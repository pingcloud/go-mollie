package types

// Resource type
// Base type for all Resources
type Resource string

const (
	// ResourceSubscription for subscription
	ResourceSubscription Resource = "subscription"
	// ResourceCustomer for customer
	ResourceCustomer Resource = "customer"
	// ResourceMandate for mandate
	ResourceMandate Resource = "mandate"
	// ResourceOrder for order
	ResourceOrder Resource = "order"
	// ResourceRefund for refund
	ResourceRefund Resource = "refund"
	// ResourceCapture for capture
	ResourceCapture Resource = "capture"
	// ResourceChargeback for chargeback
	ResourceChargeback Resource = "chargeback"
	// ResourceMethod for method
	ResourceMethod Resource = "method"
	// ResourcePayment for payment
	ResourcePayment Resource = "payment"
	// ResourceShipment for shipment
	ResourceShipment Resource = "shipment"
)

// Mode type for live/test environment
type Mode string

const (
	// ModeLive for live environment
	ModeLive Mode = "live"
	// ModeTest for test environment
	ModeTest Mode = "test"
)

// Locale type
type Locale string

const (
	// LocaleEnUs for en_US
	LocaleEnUs Locale = "en_US"
	// LocaleNlNl for nl_NL
	LocaleNlNl Locale = "nl_NL"
	// LocaleNlBe for nl_BE
	LocaleNlBe Locale = "nl_BE"
	// LocaleFrFr for fr_FR
	LocaleFrFr Locale = "fr_FR"
	// LocaleFrBe for fr_BE
	LocaleFrBe Locale = "fr_BE"
	// LocaleDeDe for de_DE
	LocaleDeDe Locale = "de_DE"
	// LocaleDeAt for de_AT
	LocaleDeAt Locale = "de_AT"
	// LocaleCeCh for de_CH
	LocaleCeCh Locale = "de_CH"
	// LocaleEsEs for es_ES
	LocaleEsEs Locale = "es_ES"
	// LocaleCaEs for ca_ES
	LocaleCaEs Locale = "ca_ES"
	// LocalePtPt for pt_PT
	LocalePtPt Locale = "pt_PT"
	// LocaleItIt for it_IT
	LocaleItIt Locale = "it_IT"
	// LocaleNbNo for nb_NO
	LocaleNbNo Locale = "nb_NO"
	// LocaleSvSe for sv_SE
	LocaleSvSe Locale = "sv_SE"
	// LocaleFiFi for fi_FI
	LocaleFiFi Locale = "fi_FI"
	// LocaleDaDk for da_DK
	LocaleDaDk Locale = "da_DK"
	// LocaleIsIs for is_IS
	LocaleIsIs Locale = "is_IS"
	// LocaleHuHu for hu_HU
	LocaleHuHu Locale = "hu_HU"
	// LocalePlPl for pl_PL
	LocalePlPl Locale = "pl_PL"
	// LocaleLvLv for lv_LV
	LocaleLvLv Locale = "lv_LV"
	// LocaleLtLt for lt_LT
	LocaleLtLt Locale = "lt_LT"
)
