package types

// SequenceType type
type SequenceType string

const (
	// SequencetypeOneoff default, one-time payment
	SequencetypeOneoff SequenceType = "oneoff"
	// SequencetypeRecurring recurring payment
	SequencetypeRecurring SequenceType = "recurring"
	// SequencetypeFirst first payment for recurring payments
	SequencetypeFirst SequenceType = "first"
)
