package types

import "fmt"

// Amount - The amount that you want to use, e.g. {"currency":"EUR", "value":"100.00"} if you would want to use €100.00.
type Amount struct {
	// Currency - An ISO 4217 currency code. The currencies supported depend on the payment methods that are enabled on
	// your account.
	Currency Currency `json:"currency,omitempty" url:"currency,omitempty"`
	// Amount - A string containing the exact amount you want to charge in the given currency. Make sure to send the
	// right amount of decimals. Non-string values are not accepted.
	Value Value `json:"value,omitempty"    url:"value,omitempty"`
}

// Value is the String representation of a float32 value
type Value string

// Currency defines all valid currencies for the mollie API
type Currency string

const (
	// CurrencyAUD for Australian dollar
	CurrencyAUD = "AUD"
	// CurrencyBGN for Bulgarian lev
	CurrencyBGN = "BGN"
	// CurrencyBRL for Brazilian real
	CurrencyBRL = "BRL"
	// CurrencyCAD for Canadian dollar
	CurrencyCAD = "CAD"
	// CurrencyCHF for Swiss franc
	CurrencyCHF = "CHF"
	// CurrencyCZK for Czech koruna
	CurrencyCZK = "CZK"
	// CurrencyDKK for Danish krone
	CurrencyDKK = "DKK"
	// CurrencyEUR for Euro
	CurrencyEUR = "EUR"
	// CurrencyGBP for British pound
	CurrencyGBP = "GBP"
	// CurrencyHKD for Hong Kong dollar
	CurrencyHKD = "HKD"
	// CurrencyHRK for Croatian kuna
	CurrencyHRK = "HRK"
	// CurrencyHUF for Hungarian forint
	CurrencyHUF = "HUF"
	// CurrencyILS for Israeli new shekel
	CurrencyILS = "ILS"
	// CurrencyISK for Icelandic króna
	CurrencyISK = "ISK"
	// CurrencyJPY for Japanese yen
	CurrencyJPY = "JPY"
	// CurrencyMXN for Mexican peso
	CurrencyMXN = "MXN"
	// CurrencyMYR for Malaysian ringgit
	CurrencyMYR = "MYR"
	// CurrencyNOK for Norwegian krone
	CurrencyNOK = "NOK"
	// CurrencyNZD for New Zealand dollar
	CurrencyNZD = "NZD"
	// CurrencyPHP for Philippine piso
	CurrencyPHP = "PHP"
	// CurrencyPLN for Polish złoty
	CurrencyPLN = "PLN"
	// CurrencyRON for Romanian leu
	CurrencyRON = "RON"
	// CurrencyRUB for Russian ruble
	CurrencyRUB = "RUB"
	// CurrencySEK for Swedish krona
	CurrencySEK = "SEK"
	// CurrencySGD for Singapore dollar
	CurrencySGD = "SGD"
	// CurrencyTHB for Thai baht
	CurrencyTHB = "THB"
	// CurrencyTWD for New Taiwan dollar
	CurrencyTWD = "TWD"
	// CurrencyUSD for United States dollar
	CurrencyUSD = "USD"
)

// NewAmount transforms a float32 value to a string value needed by the mollie API
// Example:
//  - NewAmount(9.95, CUR)
func NewAmount(value float32, currency Currency) *Amount {
	var strValue = Value(fmt.Sprintf("%.2f", value))
	return &Amount{
		Currency: CurrencyEUR,
		Value:    strValue,
	}
}
