package types

import "time"

// Date string
// Format: YYY-MM-DD
type Date string

// NewDate returns a date string in the proper format
func NewDate(time time.Time) Date {
	return Date(time.Format("2006-01-02"))
}
