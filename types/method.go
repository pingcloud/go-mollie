package types

// Method type
type Method string

const (
	// MethodCreditcard for creditcard
	MethodCreditcard Method = "creditcard"
	// MethodDirectdebit for directdebit
	MethodDirectdebit Method = "directdebit"
	// MethodBankcontact for bancontact
	MethodBankcontact Method = "bancontact"
	// MethodBanktransfer for banktransfer
	MethodBanktransfer Method = "banktransfer"
	// MethodBelfius for belfius
	MethodBelfius Method = "belfius"
	// MethodEps for eps
	MethodEps Method = "eps"
	// MethodGiftcard for giftcard
	MethodGiftcard Method = "giftcard"
	// MethodGiropay for giropay
	MethodGiropay Method = "giropay"
	// MethodInghomepay for inghomepay
	MethodInghomepay Method = "inghomepay"
	// MethodIdeal for ideal
	MethodIdeal Method = "ideal"
	// MethodKbc for kbc
	MethodKbc Method = "kbc"
	// MethodKlarnapaylater for klarnapaylater
	MethodKlarnapaylater Method = "klarnapaylater"
	// MethodKlarnasliceit for klarnasliceit
	MethodKlarnasliceit Method = "klarnasliceit"
	// MethodPaypal for paypal
	MethodPaypal Method = "paypal"
	// MethodPaysafecard for paysafecard
	MethodPaysafecard Method = "paysafecard"
	// MethodPrzelewy24 for przelewy24
	MethodPrzelewy24 Method = "przelewy24"
	// MethodSofort for sofort
	MethodSofort Method = "sofort"
	// MethodApplepay for applepay
	MethodApplepay Method = "applepay"
)

// MethodResource type
type MethodResource string

const (
	// MethodresourcePayments for resources.Payment
	MethodresourcePayments MethodResource = "payments"
	// MethodresourceOrders for resources.Order
	MethodresourceOrders MethodResource = "orders"
)
