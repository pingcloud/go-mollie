package mollie

import (
	"os"
	"testing"
)

func TestNewMollieAPIClient(t *testing.T) {

	client := NewAPIClient(os.Getenv("MOLLIE_API_KEY"))

	if client.Subsciptions == nil {
		t.Error("client.Subscription is nil, want services.SubscriptionEndpoint")
	}

	if client.Customers == nil {
		t.Error("client.Subscription is nil, want services.SubscriptionEndpoint")
	}

	if client.Payments == nil {
		t.Error("client.Payments is nil, want services.PaymentsEndpoint")
	}

	if client.Orders == nil {
		t.Error("client.Orders is nil, want services.OrdersEndpoint")
	}

	if client.Methods == nil {
		t.Error("client.Methods is nil, want services.MethodsEndpoint")
	}

	if client.Mandates == nil {
		t.Error("client.Mandates is nil, want services.MandatesEndpoint")
	}
}
