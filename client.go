package mollie

import "bitbucket.org/pingcloud/go-mollie/services"

// APIClient contains all mollie API endpoints
type APIClient struct {
	apiKey       string
	Subsciptions *services.SubscriptionEnpoint
	Customers    *services.CustomersEnpoint
	Mandates     *services.MandatesEndpoint
	Payments     *services.PaymentsEndpoint
	Methods      *services.MethodsEndpoint
	Orders       *services.OrdersEnpoint
}

// NewAPIClient returns a new mollie APIClient
func NewAPIClient(apiKey string) *APIClient {
	client := &APIClient{
		apiKey:       apiKey,
		Subsciptions: services.NewSubscriptionEndpoint(apiKey),
		Customers:    services.NewCustomerEndpoint(apiKey),
		Mandates:     services.NewMandatesEndpoint(apiKey),
		Payments:     services.NewPaymentsEndpoint(apiKey),
		Methods:      services.NewMethodsEndpoint(apiKey),
		Orders:       services.NewOrdersEndpoint(apiKey),
	}

	return client
}
