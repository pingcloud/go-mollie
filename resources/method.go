package resources

import (
	"bitbucket.org/pingcloud/go-mollie/types"
)

// Method - https://docs.mollie.com/reference/v2/methods-api/get-method#response
type Method struct {
	Resource `json:",inline"`

	// The full name of the payment method, translated in the optional locale passed.
	Description string `json:"description"`

	// An object containing value and currency. It represents the minimum payment amount required to use this payment
	// method.
	MinimumAmount types.Amount `json:"minimumAmount"`

	// An object containing value and currency. It represents the maximum payment amount allowed when using this payment
	// method.
	MaximumAmount types.Amount `json:"maximumAmount"`

	// The URLs of images representing the payment method.
	Image struct {
		// Size1x url of the image
		Size1x string `json:"size1x"`

		// Size2x url of the image
		Size2x string `json:"size2x"`

		// SVG url of the image
		SVG string `json:"svg"`
	} `json:"image"`

	// Pricing set of the payment method what will be include if you add the parameter.
	Pricing []struct {
		// The area or product-type where the pricing is applied for, translated in the optional locale passed.
		Description string `json:"description"`

		// The fixed price per transaction
		Fixed types.Amount `json:"fixed"`

		// A string containing the percentage what will be charged over the payment amount besides the fixed price.
		Variable string `json:"variable"`
	} `json:"pricing,omitempty"`
}

// MethodList contains a list of methods
type MethodList struct {
	Count    uint `json:"count"`
	Embedded struct {
		Methods []*Method `json:"methods"`
	} `json:"_embedded"`
}
