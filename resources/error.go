package resources

import "fmt"

// Error type
type Error struct {
	// Status Code (HTTP)
	Status uint `json:"status"`

	// Title of the Errir
	Title string `json:"title"`

	// Detail information on the error
	Detail string `json:"detail"`

	// Field causing the error
	Field string `json:"field,omitempty"`

	// Links for further infromation
	Links map[string]ResourceLink `json:"_links"`
}

// Error to make it errors.Error compatible
func (e Error) Error() string {
	return fmt.Sprintf("Mollie %v error: %v. (Field: %v)", e.Title, e.Detail, e.Field)
}
