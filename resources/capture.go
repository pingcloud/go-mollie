package resources

import (
	"bitbucket.org/pingcloud/go-mollie/types"
)

// Capture - https://docs.mollie.com/reference/v2/captures-api/get-capture#response
type Capture struct {
	Resource `json:",inline"`

	// The mode used to create this capture.
	Mode types.Mode `json:"mode"`

	// The amount charged back by the consumer.
	Amount types.Amount `json:"amount"`

	// This optional field will contain the amount that will be deducted from your account, converted to the currency
	// your account is settled in. It follows the same syntax as the amount property.
	//
	// Note that for chargebacks, the value key of settlementAmount will be negative.
	//
	// Any amounts not settled by Mollie will not be reflected in this amount, e.g. PayPal chargebacks.
	SettlementAmount types.Amount `json:"settlementAmount,omitempty"`

	// The unique identifier of the payment this chargeback was issued for. For example: tr_7UhSN1zuXS. The full payment
	// object can be retrieved via the payment URL in the _links object.
	PaymentID string `json:"paymentId"`

	// The unique identifier of the shipment that triggered the creation of this capture, for example: shp_3wmsgCJN4U.
	// The full shipment object can be retrieved via the shipment URL in the _links object.
	ShipmentID string `json:"shipmentId,omitempty"`

	// The unique identifier of the settlement this capture was settled with, for example: stl_jDk30akdN. The full
	// settlement object can be retrieved via the capture URL in the _links object.
	SettlementID string `json:"settlementId,omitempty"`
}

// CaptureList contains a list of captures
type CaptureList struct {
	Count    uint `json:"count"`
	Embedded struct {
		Captures []*Capture `json:"captures"`
	} `json:"_embedded"`
}
