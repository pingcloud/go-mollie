package resources

import (
	"bitbucket.org/pingcloud/go-mollie/types"
	"time"
)

// Resource is a basic resource object, user by the other resources
type Resource struct {
	// Resource identifier
	Resource types.Resource `json:"resource"`

	// ID of the object
	ID string `json:"id"`

	// CreatedAt - Date when the object was createt
	CreatedAt time.Time `json:"createdAt,omitempty"`

	// Links for further infromation
	Links map[string]ResourceLink `json:"_links"`
}

// ResourceLink - https://docs.mollie.com/guides/common-data-types#url-object
// In v2 endpoints, URLs are commonly represented as objects with an href and type field.
type ResourceLink struct {
	// Href - The actual URL string.
	Href string `json:"href"`

	// Type - The content type of the page or endpoint the URL points to.
	Type string `json:"type"`
}
