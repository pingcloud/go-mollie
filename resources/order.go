package resources

import (
	"fmt"
	"bitbucket.org/pingcloud/go-mollie/types"
	"time"
)

// Order - https://docs.mollie.com/reference/v2/orders-api/get-order#response
type Order struct {
	Resource `json:",inline"`

	// The profile the order was created on, for example pfl_v9hTwCvYqw.
	ProfileID string `json:"profileId"`

	// The payment method last used when paying for the order.
	Method types.Method `json:"method"`

	// The mode used to create this order.
	//
	// Possible values: live test
	Mode types.Mode `json:"mode"`

	// The total amount of the order, including VAT and discounts.
	Amount types.Amount `json:"amount"`

	// The amount captured, thus far. The captured amount will be settled to your account.
	//
	// For orders that have the status authorized, you must ship the order to ensure the order amount gets captured.
	AmountCaptured types.Amount `json:"amountCaptured,omitempty"`

	// The total amount refunded, thus far.
	AmountRefunded types.Amount `json:"amountRefunded,omitempty"`

	// The status of the order. One of the following values:
	//
	// - created
	// - paid
	// - authorized
	// - canceled
	// - shipping
	// - completed
	// - expired
	//
	// See Order status changes for details on the orders’ statuses.
	Status OrderStatus `json:"status"`

	// Contains the OrderLines
	Lines []*OrderLine `json:"lines"`

	// Whether or not the order can be (partially) canceled.
	IsCancelable bool `json:"isCancelable"`

	// The person and the address the order is billed to. See below.
	BillingAddress types.OrderAddress `json:"billingAddress"`

	// The date of birth of your customer, if available.
	ConsumerDateOfBirth types.Date `json:"consumerDateOfBirth,omitempty"`

	// Your order number that was used when creating the order.
	OrderNumber string `json:"orderNumber"`

	// The person and the address the order is billed to. See below.
	ShippingAddress types.OrderAddress `json:"shippingAddress,omitempty"`

	// The locale used during checkout. Note that the locale may have been changed by your customer during checkout.
	Locale types.Locale `json:"locale"`

	// Data provided during the order creation.
	Metadata map[string]interface{} `json:"metadata,omitempty"`

	// The URL your customer will be redirected to after completing or canceling the payment process.
	RedirectURL string `json:"redirectUrl,omitempty"`

	// The URL Mollie will call as soon an important status change on the order takes place.
	WebhookURL string `json:"webhookUrl,omitempty"`

	// The date and time the order will expire, in ISO 8601 format. Note that you have until this date to fully ship the
	// order.
	//
	// For some payment methods, such as Klarna Pay later this means that you will lose the authorization and not be
	// settled for the amounts of the unshipped order lines.
	//
	// The expiry period for orders is 28 days.
	ExpiresAt time.Time `json:"expiresAt,omitempty"`

	// If the order is expired, the time of expiration will be present in ISO 8601 format.
	ExpiredAt time.Time `json:"expiredAt,omitempty"`

	// If the order has been paid, the time of payment will be present in ISO 8601 format.
	PaidAt time.Time `json:"paidAt,omitempty"`

	// If the order has been authorized, the time of authorization will be present in ISO 8601 format.
	AuthorizedAt time.Time `json:"authorizedAt,omitempty"`

	// If the order has been canceled, the time of cancellation will be present in ISO 8601 format.
	CanceledAt time.Time `json:"canceledAt,omitempty"`

	// If the order is completed, the time of completion will be present in ISO 8601 format.
	CompletedAt time.Time `json:"completedAt,omitempty"`

	// An object with the requested embedded resources, such as payments, that belong to this order.
	Embedded struct {
		// An array of embedded payment resources.
		Payments []*Payment `json:"payments,omitempty"`

		// An array of embedded refunds.
		Refunds []*Refund `json:"refunds,omitempty"` // TODO: RefundsAPI
	} `json:"_embedded,omitempty"`
}

// OrderLine - https://docs.mollie.com/reference/v2/orders-api/get-order#order-line-details
type OrderLine struct {
	Resource `json:",inline"`

	// The ID of the order the line belongs too, for example ord_kEn1PlbGa.
	OrderID string `json:"orderId"`

	// The type of product bought, for example, a physical or a digital product. Will be one of the following values:
	//
	// - physical
	// - discount
	// - digital
	// - shipping_fee
	// - store_credit
	// - gift_card
	// - surcharge
	Type OrderLineType `json:"type"`

	// A description of the order line, for example LEGO 4440 Forest Police Station.
	Name string `json:"name"`

	// Status of the order line. One of the following values:
	//
	// - created
	// - authorized
	// - paid
	// - shipping
	// - canceled
	// - completed
	Status OrderLineStatus `json:"status"`

	// Whether or not the order line can be (partially) canceled.
	IsCancelable bool `json:"isCancelable"`

	// The number of items in the order line.
	Quantity uint `json:"quantity"`

	// The number of items that are shipped for this order line.
	QuantityShipped uint `json:"quantityShipped"`

	// The total amount that is shipped for this order line.
	AmountShipped *types.Amount `json:"amountShipped,omitempty"`

	// The number of items that are refunded for this order line.
	QuantityRefunded uint `json:"quantityRefunded"`

	// The total amount that is refunded for this order line.
	AmountRefunded *types.Amount `json:"amountRefunded,omitempty"`

	// The number of items that are canceled in this order line.
	QuantityCanceled uint `json:"quantityCanceled"`

	// The total amount that is canceled in this order line.
	AmountCanceled *types.Amount `json:"amountCanceled,omitempty"`

	// The number of items that can still be shipped for this order line.
	ShippableQuantity uint `json:"shippableQuantity"`

	// The number of items that can still be refunded for this order line.
	RefundableQuantity uint `json:"refundableQuantity"`

	// The number of items that can still be canceled for this order line.
	CancelableQuantity uint `json:"cancelableQuantity"`

	// The price of a single item including VAT in the order line.
	UnitPrice types.Amount `json:"unitPrice"`

	// Any discounts applied to the order line.
	DiscountAmount *types.Amount `json:"discountAmount,omitempty"`

	// The total amount of the line, including VAT and discounts.
	TotalAmount types.Amount `json:"totalAmount"`

	// The VAT rate applied to the order line, for example "21.00" for 21%. The vatRate is passed as a string and not as
	// a float to ensure the correct number of decimals are passed.
	VatRate VatRate `json:"vatRate"`

	// The amount of value-added tax on the line.
	VatAmount types.Amount `json:"vatAmount"`

	// The SKU, EAN, ISBN or UPC of the product sold.
	SKU string `json:"sku,omitempty"`

	// Data provided during the orderline creation.
	Metadata map[string]interface{} `json:"metadata,omitempty"`
}

// VatRate type
type VatRate string

// NewVatRate reeturn a vat rate in proper format.
func NewVatRate(rate float32) VatRate {
	return VatRate(fmt.Sprintf("%.2f", rate))
}

// OrderList contains a list of orders
type OrderList struct {
	Count    uint `json:"count"`
	Embedded struct {
		Orders []*Order `json:"orders"`
	} `json:"_embedded"`
}

// OrderLineType type
type OrderLineType string

const (
	// OrderLineTypePhysical for phsyical goods
	OrderLineTypePhysical OrderLineType = "physical"
	// OrderLineTypeDiscount for discounts
	OrderLineTypeDiscount OrderLineType = "discount"
	// OrderLineTypeDigital for digital good
	OrderLineTypeDigital OrderLineType = "digital"
	// OrderLineTypeShippingFee for shipping fees
	OrderLineTypeShippingFee OrderLineType = "shipping_fee"
	// OrderLineTypeStoreCredit for store credit
	OrderLineTypeStoreCredit OrderLineType = "store_credit"
	// OrderLineTypeGiftCard for gift cards
	OrderLineTypeGiftCard OrderLineType = "gift_card"
	// OrderLineTypeSurcharge for surcharges
	OrderLineTypeSurcharge OrderLineType = "surcharge"
)

// OrderStatus type
type OrderStatus string

const (
	// OrderStatusCreated for created orders
	OrderStatusCreated OrderStatus = "created"
	// OrderStatusPaid fpr paid orders
	OrderStatusPaid OrderStatus = "paid"
	// OrderStatusAuthorized for authorized orders
	OrderStatusAuthorized OrderStatus = "authorized"
	// OrderStatusCanceled for canceled orders
	OrderStatusCanceled OrderStatus = "canceled"
	// OrderStatusShipping for shipped orders
	OrderStatusShipping OrderStatus = "shipping"
	// OrderStatusCompleted for completed orders
	OrderStatusCompleted OrderStatus = "completed"
	// OrderStatusExpired for expired orders
	OrderStatusExpired OrderStatus = "expired"
)

// OrderLineStatus type
type OrderLineStatus string

const (
	// OrderLineStatusCreated for created order lines
	OrderLineStatusCreated OrderStatus = "created"
	// OrderLineStatusPaid for paid order lines
	OrderLineStatusPaid OrderStatus = "paid"
	// OrderLineStatusAuthorized  for authorized order lines
	OrderLineStatusAuthorized OrderStatus = "authorized"
	// OrderLineStatusCanceled for canceled order lines
	OrderLineStatusCanceled OrderStatus = "canceled"
	// OrderLineStatusShipping for shipped order lines
	OrderLineStatusShipping OrderStatus = "shipping"
	// OrderLineStatusCompleted for completed order lines
	OrderLineStatusCompleted OrderStatus = "completed"
)
