package resources

import (
	"bitbucket.org/pingcloud/go-mollie/types"
)

// Customer - https://docs.mollie.com/reference/v2/customers-api/get-customer#response
type Customer struct {
	Resource `json:",inline"`

	// The mode used to create this customer.
	Mode types.Mode `json:"mode"`

	// The full name of the customer as provided when the customer was created.
	Name string `json:"name"`

	// The email address of the customer as provided when the customer was created.
	Email string `json:"email"`

	// Allows you to preset the language to be used in the hosted payment pages shown to the consumer. If this parameter
	// was not provided when the customer was created, the browser language will be used instead in the payment flow
	// (which is usually more accurate).
	Locale types.Locale `json:"locale"`

	// Data provided during the customer creation.
	Metadata map[string]interface{} `json:"metadata"`
}

// CustomerList contains a list of customers
type CustomerList struct {
	Count    uint `json:"count"`
	Embedded struct {
		Customers []*Customer `json:"customers"`
	} `json:"_embedded"`
}
