package resources

// Shipment - https://docs.mollie.com/reference/v2/shipments-api/get-shipment#response
type Shipment struct {
	Resource `json:",inline"`

	// The order this shipment was created on, for example ord_8wmqcHMN4U.
	OrderID string `json:"orderId"`

	// An object containing shipment tracking details. Will be omitted when no tracking details are available.
	Tracking *Tracking `json:"tracking,omitempty"`

	// An array of order line objects as described in Get order.
	//
	// The lines will show the quantity, discountAmount, vatAmount and totalAmount shipped in this shipment. If the line
	// was partially shipped, these values will be different from the values in response from the Get order API.
	Lines []*OrderLine `json:"lines"`
}

// ShipmentList contains a list of Shipments
type ShipmentList struct {
	Count    uint `json:"count"`
	Embedded struct {
		Shipments []*Shipment `json:"shipments"`
	} `json:"_embedded"`
}

// Tracking type
type Tracking struct {
	// The name of the postal carrier.
	Carrier string `json:"carrier,omitempty"`

	// The track and trace code for the shipment.
	Code string `json:"code,omitempty"`

	// The URL where your customer can track the shipment.
	URL string `json:"url,omitempty"`
}
