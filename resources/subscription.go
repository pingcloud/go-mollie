package resources

import (
	"bitbucket.org/pingcloud/go-mollie/types"
	"time"
)

// Subscription - https://docs.mollie.com/reference/v2/subscriptions-api/get-subscription#response
type Subscription struct {
	Resource `json:",inline"`

	// The mode used to create this subscription. Mode determines whether the subscription’s payments are real or test
	// payments.
	Mode types.Mode `json:"mode"`

	// The subscription’s current status, depends on whether the customer has a pending, valid or invalid mandate.
	Status SubscriptionStatus `json:"status"`

	// The constant amount that is charged with each subscription payment, e.g. {"currency":"EUR", "value":"10.00"}
	// for a €10.00 subscription.
	Amount types.Amount `json:"amount"`

	// Total number of charges for the subscription to complete.
	Times uint `json:"times"`

	// Number of charges left for the subscription to complete.
	TimesRemaining uint `json:"timesRemaining"`

	// Interval to wait between charges, for example 1 month or 14 days.
	//
	// Possible values: ... months ... weeks ... days
	Interval types.Interval `json:"interval"`

	// The start date of the subscription in YYYY-MM-DD format.
	StartDate types.Date `json:"startDate"`

	// The date of the next scheduled payment in YYYY-MM-DD format. When there will be no next payment, for example when
	// the subscription has ended, this parameter will not be returned.
	NextPaymentDate types.Date `json:"nextPaymentDate,omitempty"`

	// The description specified during subscription creation. This will be included in the payment description along
	// with the charge date in YYYY-MM-DD format.
	Description string `json:"description"`

	// The payment method used for this subscription, either forced on creation or null if any of the customer’s valid
	// mandates may be used.
	//
	// Possible values: creditcard directdebit null
	Method string `json:"method,omitempty"`

	// The mandate used for this subscription. When there is no mandate specified, this parameter will not be returned.
	MandateID string `json:"mandateId,omitempty"`

	// The subscription’s date and time of cancellation, in ISO 8601 format. This parameter is omitted if the payment is
	// not canceled (yet).
	CanceledAt time.Time `json:"canceledAt"`

	// The URL Mollie will call as soon a payment status change takes place.
	WebhookURL string `json:"webhook_url,omitempty"`

	// The optional metadata you provided upon subscription creation. Metadata can for example be used to link a plan to
	// a subscription.
	Metadata map[string]interface{} `json:"metadata"`

	// ApplicationFee - The application fee, if the subscription was created with one. This will be applied on each
	// payment created for the subscription.
	ApplicationFee struct {
		// Amount - The application fee amount in EUR as specified during subscription creation.
		Amount float32 `json:"amount"`

		// Description - The description of the application fee as specified during subscription creation.
		Description string `json:"description"`
	} `json:"applicationFee"`
}

const (
	// SubscriptionstatusPending for pending subscription
	SubscriptionstatusPending SubscriptionStatus = "pending"
	// SubscriptionstatusActive for active subscription
	SubscriptionstatusActive SubscriptionStatus = "active"
	// SubscriptionstatusCanceled for canceled subscription
	SubscriptionstatusCanceled SubscriptionStatus = "canceled"
	// SubscriptionstatusSuspended for suspended subscription
	SubscriptionstatusSuspended SubscriptionStatus = "suspended"
	// SubscriptionstatusCompleted for completed subscription
	SubscriptionstatusCompleted SubscriptionStatus = "completed"
)

const (
	// SubscriptionmethodCreditcard represents method "creditcard"
	SubscriptionmethodCreditcard = SubscriptionMethod(types.MethodCreditcard)
	// SubscriptionmethodDirectdebit represents method "directdebit"
	SubscriptionmethodDirectdebit = SubscriptionMethod(types.MethodDirectdebit)
)

// SubscriptionStatus type
type SubscriptionStatus string

// SubscriptionMethod type
// since not all methods are allowed
type SubscriptionMethod string

// SubscriptionList contains a list of Subscriptions
type SubscriptionList struct {
	Count    uint `json:"count"`
	Embedded struct {
		Subscriptions []*Subscription `json:"subscriptions"`
	} `json:"_embedded"`
}
