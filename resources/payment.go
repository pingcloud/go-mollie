package resources

import (
	"bitbucket.org/pingcloud/go-mollie/types"
	"time"
)

// Payment - https://docs.mollie.com/reference/v2/payments-api/get-payment#response
type Payment struct {
	Resource `json:",inline"`

	// The mode used to create this payment. Mode determines whether a payment is real (live mode) or a test payment.
	Mode types.Mode `json:"mode"`

	// The payment’s status. Please refer to the documentation regarding statuses for more info about which statuses
	// occur at what point.
	Status PaymentStatus `json:"status"`

	// Whether or not the payment can be canceled.
	IsCancelable bool `json:"isCancelable"`

	// The date and time the payment became authorized, in ISO 8601 format. This parameter is omitted if the payment is
	// not authorized (yet).
	AuthorizedAt time.Time `json:"authorizedAt,omitempty"`

	// The date and time the payment became paid, in ISO 8601 format. This parameter is omitted if the payment is not
	// completed (yet).
	PaidAt time.Time `json:"paidAt,omitempty"`

	// The date and time the payment was canceled, in ISO 8601 format. This parameter is omitted if the payment is not
	// canceled (yet).
	CanceledAt time.Time `json:"canceledAt,omitempty"`

	// The date and time the payment will expire, in ISO 8601 format.
	ExpiresAt time.Time `json:"expiresAt"`

	// The date and time the payment was expired, in ISO 8601 format. This parameter is omitted if the payment did not
	// expire (yet).
	ExpiredAt time.Time `json:"expiredAt,omitempty"`

	// The date and time the payment failed, in ISO 8601 format. This parameter is omitted if the payment did not fail
	// (yet).
	FailedAt time.Time `json:"failedAt,omitempty"`

	// The amount of the payment, e.g. {"currency":"EUR", "value":"100.00"} for a €100.00 payment.
	Amount types.Amount `json:"amount"`

	// The total amount that is already refunded. Only available when refunds are available for this payment. For some
	// payment methods, this amount may be higher than the payment amount, for example to allow reimbursement of the
	// costs for a return shipment to the customer.
	AmountRefunded types.Amount `json:"amountRefunded,omitempty"`

	// The remaining amount that can be refunded. Only available when refunds are available for this payment.
	AmountRemaining types.Amount `json:"amountRemaining,omitempty"`

	// The total amount that is already captured for this payment. Only available when this payment supports captures.
	AmountCaptured types.Amount `json:"amountCaptured,omitempty"`

	// A short description of the payment. The description is visible in the Dashboard and will be shown on the
	// customer’s bank or card statement when possible.
	Description string `json:"description"`

	// The URL your customer will be redirected to after completing or canceling the payment process.
	RedirectURL string `json:"redirectUrl,omitempty"`

	// The URL Mollie will call as soon an important status change takes place.
	WebhookURL string `json:"webhook_url,omitempty"`

	// The payment method used for this payment, either forced on creation by specifying the method parameter, or chosen
	// by the customer on our payment method selection screen.
	//
	// If the payment is only partially paid with a gift card, the method remains giftcard.
	Method types.Method `json:"method"`

	// The optional metadata you provided upon payment creation. Metadata can for example be used to link an order to a
	// payment.
	Metadata map[string]interface{} `json:"metadata"`

	// The customer’s locale, either forced on creation by specifying the locale parameter, or detected by us during
	// checkout. Will be a full locale, for example nl_NL.
	Locale types.Locale `json:"locale"`

	// This optional field contains your customer’s ISO 3166-1 alpha-2 country code, detected by us during checkout. For
	// example: BE. This field is omitted if the country code was not detected.
	CountryCode string `json:"countryCode,omitempty"` // TODO : TYPE!!!

	// The identifier referring to the profile this payment was created on. For example, pfl_QkEhN94Ba.
	ProfileID string `json:"profileId"`

	// This optional field will contain the amount that will be settled to your account, converted to the currency your
	// account is settled in. It follows the same syntax as the amount property.
	//
	// Any amounts not settled by Mollie will not be reflected in this amount, e.g. PayPal or gift cards.
	SettlementAmount *types.Amount `json:"settlementAmount,omitempty"`

	// The identifier referring to the settlement this payment was settled with. For example, stl_BkEjN2eBb.
	SettlementID string `json:"settlementId,omitempty"`

	// If a customer was specified upon payment creation, the customer’s token will be available here as well. For
	// example, cst_XPn78q9CfT.
	CustomerID string `json:"customer_id,omitempty"`

	// Indicates which type of payment this is in a recurring sequence. Set to first for first payments that allow the
	// customer to agree to automatic recurring charges taking place on their account in the future. Set to recurring
	// for payments where the customer’s card is charged automatically.
	//
	// Set to oneoff by default, which indicates the payment is a regular non-recurring payment.
	//
	// Possible values: oneoff first recurring
	SequenceType types.SequenceType `json:"sequenceType"`

	// If the payment is a first or recurring payment, this field will hold the ID of the mandate.
	MandatID string `json:"mandatId,omitempty"`

	// When implementing the Subscriptions API, any recurring charges resulting from the subscription will hold the ID
	// of the subscription that triggered the payment.
	SubscriptionID string `json:"subscriptionId,omitempty"`

	// If the payment was created for an order, the ID of that order will be part of the response.
	OrderID string `json:"orderId,omitempty"`

	// The application fee, if the payment was created with one.
	ApplicationFee struct {
		// The application fee amount as specified during payment creation.
		Amount *types.Amount `json:"amount,omitempty"`

		// The description of the application fee as specified during payment creation.
		Description string `json:"description,omitempty"`
	} `json:"applicationFee,omitempty"`

	// Refunds and Chargebacks
	Embedded struct {
		Chargebacks []*Chargeback `json:"chargebacks,omitempty"`
		Refunds     []*Refund     `json:"refunds,omitempty"`
	} `json:"_embedded,omitempty"`

	Details struct {
		QrCode struct {
			Height int `json:"height,omitempty`
			Width int `json:"width,omitempty`
			Src string `json:"src,omitempty`
		} `json:"qrCode,omitempty"`
	} `json:"details,omitempty"`
}

// PaymentStatus type
type PaymentStatus string

const (
	// PaymentstatusOpen for open paiments
	PaymentstatusOpen PaymentStatus = "open"
	// PaymentstatusCanceled for canceled payments
	PaymentstatusCanceled PaymentStatus = "canceled"
	// PaymentstatusPending for pending payments
	PaymentstatusPending PaymentStatus = "pending"
	// PaymentstatusAuthorized for authorized payments
	PaymentstatusAuthorized PaymentStatus = "authorized"
	// PaymentstatusExpired for epired payments
	PaymentstatusExpired PaymentStatus = "expired"
	// PaymentstatusFailed for failed payments
	PaymentstatusFailed PaymentStatus = "failed"
	// PaymentstatusPaid for paid payments
	PaymentstatusPaid PaymentStatus = "paid"
)

// PaymentList contains a list of payments
type PaymentList struct {
	Count    uint `json:"count"`
	Embedded struct {
		Payments []*Payment `json:"payments"`
	} `json:"_embedded"`
}
