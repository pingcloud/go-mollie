package resources

import (
	"bitbucket.org/pingcloud/go-mollie/types"
)

// Mandate - https://docs.mollie.com/reference/v2/mandates-api/get-mandate
type Mandate struct {
	Resource `json:",inline"`

	// The mode used to create this mandate.
	Mode types.Mode `json:"mode"`

	// The status of the mandate. Please note that a status can be pending for subscription mandates when there is no
	// first payment. See our subscription guide.
	Status MandateStatus `json:"status"`

	// Payment method of the mandate.
	Method MandateMethod `json:"method"`

	// The mandate detail object contains different fields per payment method.
	//
	// For direct debit mandates, the following details are returned:
	Details struct {
		// For direct debit mandates, the following details are returned:

		// The account holder’s name.
		ConsumerName string `json:"consumerName,omitempty"`
		// The account holder’s IBAN.
		ConsumerAccount string `json:"consumerAccount,omitempty"`
		// The account holder’s bank’s BIC.
		ConsumerBIC string `json:"consumerBic,omitempty"`

		// For credit card mandates, the following details are returned:

		// The credit card holder’s name.
		CardHolder string `json:"cardHolder,omitempty"`
		// The last four digits of the credit card number.
		CardNumber string `json:"cardNumber,omitempty"`
		// The credit card’s label. Note that not all labels can be processed through Mollie.
		CardLabel CardLabel `json:"cardLabel,omitempty"`
		// Unique alphanumeric representation of the credit card, usable for identifying returning customers.
		CardFingerprint string `json:"cardFingerprint,omitempty"`
		// Expiry date of the credit card in YYYY-MM-DD format.
		CardExpiryDate types.Date `json:"cardExpiryDate,omitempty"`
	} `json:"details"`

	// Id of the mandat's customer
	CustomerID string `json:"customerId"`

	// The mandate’s custom reference, if this was provided when creating the mandate.
	MandateReference string `json:"mandateReference,omitempty"`

	// The signature date of the mandate in YYYY-MM-DD format.
	SignatureDate types.Date `json:"signatureDate"`
}

// MandateList contains a list with mandates
type MandateList struct {
	Count    uint `json:"count"`
	Embedded struct {
		Mandates []*Mandate `json:"mandates"`
	} `json:"_embedded"`
}

// CardLabel type
type CardLabel string

// MandateMethod type
type MandateMethod string

//MandateStatus type
type MandateStatus string

const (
	// MandatestatusPending represents status "pending"
	MandatestatusPending MandateStatus = "pending"
	// MandatestatusValid represents status "valid"
	MandatestatusValid MandateStatus = "valid"
	// MandatestatusInvalid represents status "invalid"
	MandatestatusInvalid MandateStatus = "invalid"
)
const (
	// MandatemethodCreditcard represents mathod "creditcard"
	MandatemethodCreditcard = MandateMethod(types.MethodCreditcard)
	// MandatemethodDirectdebit represents mathod "directdebit"
	MandatemethodDirectdebit = MandateMethod(types.MethodDirectdebit)
)
const (
	// CardlabelAmericanaxpress represents "American Express"
	CardlabelAmericanaxpress CardLabel = "American Express"
	// CardlabelCartasi represents "Carta Si"
	CardlabelCartasi CardLabel = "Carta Si"
	// CardlabelCartebleue represents"Carte Bleue"
	CardlabelCartebleue CardLabel = "Carte Bleue"
	// CardlabelDankort represents"Dankort"
	CardlabelDankort CardLabel = "Dankort"
	// CardlabelDinersclub represents "Diners Club"
	CardlabelDinersclub CardLabel = "Diners Club"
	// CardlabelDiscover represents "Discover"
	CardlabelDiscover CardLabel = "Discover"
	// CardlabelJCB represents "JCB"
	CardlabelJCB CardLabel = "JCB"
	// CardlabelLaser represents "Laser"
	CardlabelLaser CardLabel = "Laser"
	// CardlabelMaestro represents "Maestro"
	CardlabelMaestro CardLabel = "Maestro"
	// CardlabelMastercard represents "Mastercard"
	CardlabelMastercard CardLabel = "Mastercard"
	// CardlabelUnionpay represents "Unionpay"
	CardlabelUnionpay CardLabel = "Unionpay"
	// CardlabelVisa represents "Visa"
	CardlabelVisa CardLabel = "Visa"
)
