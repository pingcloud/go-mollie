package resources

import "bitbucket.org/pingcloud/go-mollie/types"

// Refund - https://docs.mollie.com/reference/v2/refunds-api/get-refund#response
type Refund struct {
	Resource `json:",inline"`

	// The amount refunded to your customer with this refund.
	Amount types.Amount `json:"amount"`

	// This optional field will contain the amount that will be deducted from your account balance, converted to the
	// currency your account is settled in. It follows the same syntax as the amount property.
	//
	// Note that for refunds, the value key of settlementAmount will be negative.
	//
	// Any amounts not settled by Mollie will not be reflected in this amount, e.g. PayPal refunds.
	//
	// Queued refunds in non EUR currencies will not have a settlement amount until they become pending.
	SettlementAmount types.Amount `json:"settlementAmount,omitempty"`

	// The description of the refund that may be shown to your customer, depending on the payment method used.
	Description string `json:"description"`

	// The optional metadata you provided upon refund creation. Metadata can for example be used to link an bookkeeping
	// ID to a refund.
	Metadata map[string]interface{} `json:"metadata,omitempty"`

	// Since refunds may be delayed for certain payment methods, the refund carries a status field.
	//
	// Possible values:
	//
	// - queued The refund will be processed once you have enough balance. You can still cancel this refund.
	//
	// - pending The refund will be processed soon (usually the next business day). You can still cancel this refund.
	//
	// - processing The refund is being processed. Cancellation is no longer possible.
	//
	// - refunded The refund has been paid out to your customer.
	//
	// - failed The refund has failed during processing.
	Status RefundStatus `json:"status"`

	// An array of order line objects as described in Get order.
	//
	// The lines will show the quantity, discountAmount, vatAmount and totalAmount refunded. If the line was partially
	// refunded, these values will be different from the values in response from the Get order API.
	//
	// Only available if the refund was created via the Create Order Refund API.
	Lines []*OrderLine `json:"lines,omitempty"`

	// The unique identifier of the payment this refund was created for. For example: tr_7UhSN1zuXS. The full payment
	// object can be retrieved via the payment URL in the _links object.
	PaymentID string `json:"paymentId"`

	// The unique identifier of the order this refund was created for. For example: ord_8wmqcHMN4U. Not present if the
	// refund was not created for an order.
	OrderID string `json:"orderId,omitempty"`
}

// RefundStatus type
type RefundStatus string

const (
	// RefundStatusQueued for queued refunds
	RefundStatusQueued RefundStatus = "queued"
	// RefundStatusPending for pednning refunds
	RefundStatusPending RefundStatus = "pending"
	// RefundStatusProcessing for processing refunds
	RefundStatusProcessing RefundStatus = "processing"
	// RefundStatusRefunded for refunded refunds
	RefundStatusRefunded RefundStatus = "refunded"
	// RefundStatusFailed for failed refunds
	RefundStatusFailed RefundStatus = "failed"
)

// RefundList contains a list of refunds
type RefundList struct {
	Count    uint `json:"count"`
	Embedded struct {
		Refunds []*Refund `json:"refunds"`
	} `json:"_embedded"`
}
