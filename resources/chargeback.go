package resources

import (
	"bitbucket.org/pingcloud/go-mollie/types"
	"time"
)

// Chargeback - https://docs.mollie.com/reference/v2/chargebacks-api/get-chargeback#response
type Chargeback struct {
	Resource `json:",inline"`

	// The amount charged back by the consumer.
	Amount types.Amount `json:"amount"`

	// This optional field will contain the amount that will be deducted from your account, converted to the currency
	// your account is settled in. It follows the same syntax as the amount property.
	//
	// Note that for chargebacks, the value key of settlementAmount will be negative.
	//
	// Any amounts not settled by Mollie will not be reflected in this amount, e.g. PayPal chargebacks.
	SettlementAmount types.Amount `json:"settlementAmount,omitempty"`

	// The date and time the chargeback was reversed if applicable, in ISO 8601 format.
	ReversedAt time.Time `json:"reversedAt,omitempty"`

	// The unique identifier of the payment this chargeback was issued for. For example: tr_7UhSN1zuXS. The full payment
	// object can be retrieved via the payment URL in the _links object.
	PaymentID string `json:"paymentId"`
}

// ChargebackList contains a list of chargebacks
type ChargebackList struct {
	Count    uint `json:"count"`
	Embedded struct {
		Chargebacks []*Chargeback `json:"chargebacks"`
	} `json:"_embedded"`
}
